#ifndef MYPROJECT_GARLIC_H
#define MYPROJECT_GARLIC_H

#include "SphereWithPolInterface.h"

class Garlic : public SphereWithPolInterface {
public:
    explicit Garlic(const Point3D& start_point, double radius, int latitudes_count, int longitudes_count);
};


#endif //MYPROJECT_GARLIC_H
