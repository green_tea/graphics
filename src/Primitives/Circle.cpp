#include "Circle.h"

Circle::Circle(Point start_point, Point finish_point) : point(start_point), radius(start_point.diff(finish_point)) {}

std::vector<Point> Circle::getBresenham() {
    std::vector<Point> points;

    int center_x = getCenterXInt();
    int center_y = getCenterYInt();

    int inner_radius = getRadiusInt();

    int x = 0;
    int y = inner_radius;

    int delta_big = (1 - inner_radius);

    while (x <= y) {
        points.emplace_back(center_x + x, center_y + y);
        points.emplace_back(center_x + x, center_y - y);
        points.emplace_back(center_x + y, center_y + x);
        points.emplace_back(center_x + y, center_y - x);
        points.emplace_back(center_x - x, center_y + y);
        points.emplace_back(center_x - x, center_y - y);
        points.emplace_back(center_x - y, center_y + x);
        points.emplace_back(center_x - y, center_y - x);

        if (delta_big <= 0) {
            delta_big += 2 * x + 1;
        } else {
            delta_big += 2 * x + 3 - 2 * y;
            --y;
        }

        ++x;
    }

    return points;
}

double Circle::getRadius() const {
    return radius;
}

double Circle::getCenterX() const {
    return point.getX();
}

double Circle::getCenterY() const {
    return point.getY();
}

int Circle::getRadiusInt() const {
    return static_cast<int>(std::round(getRadius()));
}

int Circle::getCenterXInt() const {
    return point.getIntX();
}

int Circle::getCenterYInt() const {
    return point.getIntY();
}

Circle::Circle(Point point, float radius) {
    this->point = point;
    this->radius = radius;
}


