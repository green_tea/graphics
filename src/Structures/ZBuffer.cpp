#include "ZBuffer.h"

void ZBuffer::processFigure(Bitmap *bitmap, const Camera& camera, Object3DInterface *object, bool with_intensity) {
    Color* color = object->getColor();

    if (color == nullptr) {
        return;
    }

    std::vector<Face> faces = object->getVisibleFaces(camera);
    auto center_point = object->getCenterPoint(camera);

    for (const Face& face : faces) {
        drawFace(bitmap, face, *color, camera, with_intensity, center_point);
    }
}

void ZBuffer::drawFace(Bitmap *bitmap, Face face, const Color &color, const Camera& camera, bool with_intensity, const Point3D& center_point) {
    face.sortPoints([](const Point3D& lhs, const Point3D& rhs) {
        return lhs.getY() > rhs.getY();
    });

    if (isEqual(face.getPoint(0).getY(), face.getPoint(1).getY())) {
        fillTopTriangle(bitmap, face, color, camera, with_intensity);
    } else if (isEqual(face.getPoint(1).getY(), face.getPoint(2).getY())) {
        fillBottomTriangle(bitmap, face, color, camera, with_intensity);
    } else {
        Point3D p3 = generateSplitPoint(face.getPoint(0), face.getPoint(1), face.getPoint(2));

        Face face1(face.getPoint(1), p3, face.getPoint(2));
        Face face2(face.getPoint(0), face.getPoint(1), p3);

        if (face1.calcValue(center_point) < 0) {
            face1.rotate();
        }

        if (face2.calcValue(center_point) < 0) {
            face2.rotate();
        }

        fillTopTriangle(bitmap, face1, color, camera, with_intensity);
        fillBottomTriangle(bitmap, face2, color, camera, with_intensity);
    }
}

bool ZBuffer::isEqual(double a, double b) {
    return fabs(a - b) <= EPS;
}

void ZBuffer::fillTopTriangle(Bitmap *bitmap, Face face, const Color &color, const Camera& camera, bool with_intensity) {
    if (face.getPoint(0).getX() > face.getPoint(1).getX()) {
        face.swapPoints(0, 1);
    }

    auto p1 = face.getPoint(0);
    auto p2 = face.getPoint(1);
    auto p3 = face.getPoint(2);

    auto steps_left = getSteps(p1, p3);
    auto steps_right = getSteps(p2, p3);

    double x_left = p3.getX();
    double x_right = p3.getX();

    double z_left = p3.getZ();
    double z_right = p3.getZ();

    for (int y = p3.getIntY() + 1; y >= p1.getIntY() - 1; --y) {
        for (double x = x_left; x <= x_right; x += step) {
            if (!bitmap->isInsideDoubles(x, y)) {
                continue;
            }

            if (x < std::min(p1.getX(), p3.getX())) {
                continue;
            }

            if (x > std::max(p2.getX(), p3.getX())) {
                break;
            }

            double z = z_left + (x - x_left) * (z_right - z_left) / (x_right - x_left);
            int int_x = static_cast<int>(x);
            if (bitmap->getDepth(int_x, y) < z) {
                bitmap->setDepth(int_x, y, z);
                if (with_intensity) {
                    bitmap->setPixel({ int_x, y }, color.withIntensity(camera.getIntensity(face, { x, static_cast<double>(y), z })));
                } else {
                    bitmap->setPixel({ int_x, y }, color);
                }
            }
        }

        x_left -= steps_left.first;
        x_right -= steps_right.first;

        z_left -= steps_left.second;
        z_right -= steps_right.second;
    }
}

void ZBuffer::fillBottomTriangle(Bitmap *bitmap, Face face, const Color &color, const Camera& camera, bool with_intensity) {
    if (face.getPoint(1).getX() > face.getPoint(2).getX()) {
        face.swapPoints(1, 2);
    }

    auto p1 = face.getPoint(0);
    auto p2 = face.getPoint(1);
    auto p3 = face.getPoint(2);

    auto steps_left = getSteps(p1, p2);
    auto steps_right = getSteps(p1, p3);

    double x_left = p1.getX();
    double x_right = p1.getX();

    double z_left = p1.getZ();
    double z_right = p1.getZ();

    for (int y = p1.getIntY() - 1; y <= p2.getIntY() + 1; ++y) {
        for (double x = x_left; x <= x_right; x += step) {
            if (!bitmap->isInsideDoubles(x, y)) {
                continue;
            }

            if (x < std::min(p2.getX(), p1.getX())) {
                continue;
            }

            if (x > std::max(p1.getX(), p3.getX())) {
                break;
            }

            double z = z_left + (x - x_left) * (z_right - z_left) / (x_right - x_left);
            int int_x = static_cast<int>(x);
            if (bitmap->getDepth(int_x, y) < z) {
                bitmap->setDepth(int_x, y, z);
                if (with_intensity) {
                    bitmap->setPixel({ int_x, y }, color.withIntensity(camera.getIntensity(face, { x, static_cast<double>(y), z })));
                } else {
                    bitmap->setPixel({ int_x, y }, color);
                }
            }
        }

        x_left += steps_left.first;
        x_right += steps_right.first;

        z_left += steps_left.second;
        z_right += steps_right.second;
    }
}

Point3D ZBuffer::generateSplitPoint(const Point3D &p1, const Point3D &p2, const Point3D &p3) {
    return {
            p1.getX() + (p2.getY() - p1.getY()) * (p3.getX() - p1.getX()) / (p3.getY() - p1.getY()),
            p2.getY(),
            p1.getZ() + (p3.getZ() - p1.getZ()) * (p2.getY() - p1.getY()) / (p3.getY() - p1.getY()),
    };
}

std::pair<double, double> ZBuffer::getSteps(const Point3D& p1, const Point3D& p2) const {
    double step_x = (p2.getX() - p1.getX()) / (p2.getY() - p1.getY());
    double step_z = (p2.getZ() - p1.getZ()) / (p2.getY() - p1.getY());

    return std::make_pair(step_x, step_z);
}

