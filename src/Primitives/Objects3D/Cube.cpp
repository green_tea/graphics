#include "Cube.h"

std::vector<Line> Cube::getLines(const Camera& camera) const {
    auto points = getPoints(camera);

    return {
        { points[0], points[1] },
        { points[1], points[2] },
        { points[2], points[3] },
        { points[3], points[0] },

        { points[0], points[4] },
        { points[1], points[5] },
        { points[2], points[6] },
        { points[3], points[7] },

        { points[4], points[5] },
        { points[5], points[6] },
        { points[6], points[7] },
        { points[7], points[4] },
    };
}

Cube::Cube(Point3D point) {
    this->start_point = point;
    this->points = {
            { 0, 0, 0 },
            { 0, size, 0 },
            { size, size, 0 },
            { size, 0, 0 },
            { size, 0, size },
            { 0, 0, size },
            { 0, size, size },
            { size, size, size }
    };
}

std::vector<Face> Cube::getFaces(const Camera &camera) const {
    auto points = getPointsWithoutProjection(camera);

    return {
        { points[0], points[1], points[2] },
        { points[0], points[2], points[3] },

        { points[0], points[3], points[5] },
        { points[5], points[3], points[4] },

        { points[4], points[2], points[7] },
        { points[4], points[3], points[2] },

        { points[1], points[6], points[2] },
        { points[2], points[6], points[7] },

        { points[7], points[6], points[5] },
        { points[5], points[4], points[7] },

        { points[1], points[5], points[6] },
        { points[1], points[0], points[5] }
    };
}
