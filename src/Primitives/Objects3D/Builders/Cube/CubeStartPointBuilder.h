#ifndef MYPROJECT_TETRAHEDRONSTARTPOINTBUILDER_H
#define MYPROJECT_STARTPOINTBUILDER_H

#include "../BuilderInterface.h"
#include "../../Cube.h"

class CubeStartPointBuilder : public BuilderInterface {
public:
    void setStartPoint(const Point3D& point) override;
};


#endif //MYPROJECT_TETRAHEDRONSTARTPOINTBUILDER_H
