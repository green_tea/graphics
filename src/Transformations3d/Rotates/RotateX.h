#ifndef MYPROJECT_ROTATEX_H
#define MYPROJECT_ROTATEX_H

#include "Rotate.h"

#include <cmath>

class RotateX : public Rotate {
public:
    Matrix<double> process(Matrix<double> value) const override;
};

#endif //MYPROJECT_ROTATEX_H