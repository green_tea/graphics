#include "Window3d.h"

sf::RenderWindow &Window3d::getWindow() {
    return this->window;
}

void Window3d::rightMousePress() {
}

void Window3d::rightMousePull() {
}

void Window3d::drawPanel() {
    ImGui::Begin("Control Panel");

//    if (current_object && current_object->sequenceStarted()) {
        if (cur_time % sequence_timer == 0) {
            redraw();
        }

        ++cur_time;
//    }

    if (!sequences_start) {

        if (adding_figures) {
            drawAddingFiguresButtons();
        }

        if (set_colors_start) {
            drawColorButtons();
        }

        if (ImGui::Button("draw lines")) {
            this->lines_visible = !lines_visible;
        }

        if (ImGui::Button("turn on/off light")) {
            this->light_turn_on = !light_turn_on;
        }

        if (!set_colors_start) {
            if (ImGui::Button("Set color")) {
                clearTransformations();
                set_colors_start = true;
            }
        }

        if (!adding_figures) {
            if (ImGui::Button("Add figure")) {
                clearTransformations();
                this->adding_figures = true;
            }
        }

        for (int i = 0; i < objects.size(); ++i) {
            std::stringstream s;
            s << "Select object # " << i;
            if (ImGui::Button(s.str().c_str())) {
                current_object = objects[i];
                current_x_translate = current_x_translates[i];
            }
        }

        if (!rotates_start) {
            if (ImGui::Button("Go to rotates")) {
                clearTransformations();
                this->rotates_start = true;
            }
        }

        if (!translates_start) {
            if (ImGui::Button("Go to Translates")) {
                clearTransformations();
                this->translates_start = true;
            }
        }

        if (!resizes_start) {
            if (ImGui::Button("Go to Resizes")) {
                clearTransformations();
                this->resizes_start = true;
            }
        }

        if (ImGui::Button("Go to Sequences")) {
            clearTransformations();
            sequences_start = true;
        }
    }

    if (sequences_start) {
        drawSequencesButtons();
    }

    if (translates_start) {
        drawTranslatesButtons();
    }

    if (this->rotates_start) {
        drawRotatesButtons();
    }

    if (this->resizes_start) {
        drawResizesButtons();
    }

    if (ImGui::Button("Change Projection")) {
        camera->perspectiveEnabled = !camera->perspectiveEnabled;
        redraw();
    }

    if (ImGui::Button("Move Camera >")) {
        camera->perspectiveCoefficient += default_projection_coef_shift;
        redraw();
    }

    if (ImGui::Button("Move Camera <")) {
        if (camera->perspectiveCoefficient > 200) {
            camera->perspectiveCoefficient -= default_projection_coef_shift;
            redraw();
        }
    }

    if (ImGui::Button("Clear")) {
        this->clear();
    }

    ImGui::End();

    texture.update(this->bitmap->getPixels());
    window.draw(sprite);

    drawLines();
    drawCircles();

    ImGui::SFML::Render(window);
    window.display();
}

void Window3d::update(sf::Time time) {
    ImGui::SFML::Update(window, time);
}

void Window3d::processEvent(sf::Event event) {
    ImGui::SFML::ProcessEvent(event);
}

void Window3d::clear() {
    this->lines.clear();
    this->circles.clear();
    circles.emplace_back( center_point.getProjection(), 3);

    for (int i = 0; i < objects.size(); ++i) {
        delete objects[i];
        delete current_x_translates[i];
    }
    objects.clear();
    current_x_translates.clear();
}

void Window3d::clearPixels() {
    for (int i = 0; i < WIDTH * HEIGHT * 4; i += 4) {
        this->bitmap->setIndex(i, {0, 150, 0, 100});
    }
}

void Window3d::drawLines() {
    for (const auto& line : lines) {
        window.draw(new sf::Vertex[2]{
                sf::Vertex(line.getStartPoint().getVector2f(), Color::getRed().getSFColor()),
                sf::Vertex(line.getFinishPoint().getVector2f(), Color::getRed().getSFColor()),
        }, 2, sf::Lines);
    }
}

void Window3d::makeTransformation(const std::string &transformation) {
    if (transformation == "RotateX>") {
        this->current_object->rotateX(default_rotate_angle);
    } else if (transformation == "RotateX<") {
        this->current_object->rotateX(-default_rotate_angle);
    } else if (transformation == "RotateY<") {
        this->current_object->rotateY(-default_rotate_angle);
    } else if (transformation == "RotateY>") {
        this->current_object->rotateY(default_rotate_angle);
    } else if (transformation == "RotateZ<") {
        this->current_object->rotateZ(-default_rotate_angle);
    } else if (transformation == "RotateZ>") {
        this->current_object->rotateZ(default_rotate_angle);
    } else if (transformation == "TranslateX>") {
        this->current_object->translate({ default_translate, 0, 0 });
        *current_x_translate += default_translate;
    } else if (transformation == "TranslateX<") {
        this->current_object->translate({ -default_translate, 0, 0 });
        *current_x_translate -= default_translate;
    } else if (transformation == "TranslateY<") {
        this->current_object->translate({ 0, -default_translate, 0 });
    } else if (transformation == "TranslateY>") {
        this->current_object->translate({ 0, default_translate, 0 });
    } else if (transformation == "TranslateZ<") {
        this->current_object->translate({ 0, 0, -default_translate });
    } else if (transformation == "TranslateZ>") {
        this->current_object->translate({ 0, 0, default_translate });
    } else if (transformation == "ResizeX>") {
        this->current_object->resize({ default_resize, 1, 1 });
    } else if (transformation == "ResizeX<") {
        this->current_object->resize({ 1.0 / default_resize, 1, 1 });
    } else if (transformation == "ResizeY<") {
        this->current_object->resize({ 1, 1.0 / default_resize, 1 });
    } else if (transformation == "ResizeY>") {
        this->current_object->resize({ 1, default_resize, 1 });
    } else if (transformation == "ResizeZ<") {
        this->current_object->resize({ 1, 1, 1.0 / default_resize });
    } else if (transformation == "ResizeZ>") {
        this->current_object->resize({ 1, 1, default_resize });
    } else if (transformation == "Resize>") {
        this->current_object->resize({ default_resize, default_resize, default_resize });
    } else if (transformation == "Resize<") {
        this->current_object->resize({ 1.0 / default_resize, 1.0 / default_resize, 1.0 / default_resize });
    }

    redraw();
}

void Window3d::draw3dObject(const std::string& figure, const std::string& type) {
//    clear();

    director3D.setBuilder(builder_mapper[std::make_pair(figure, type)]);
    ParamsInterface* params = new StartPointParams(center_point);

    current_object = director3D.make("start_point", params);
    objects.push_back(current_object);
    current_x_translate = new double(0);
    current_x_translates.push_back(current_x_translate);

    adding_figures = false;

    redraw();
}

void Window3d::drawCircles() {
    for (const auto& circle : circles) {
        sf::CircleShape circle_shape(circle.getRadius());
        circle_shape.setFillColor(sf::Color(0, 255, 0, 0));
        circle_shape.setPosition(circle.getCenterX() - circle.getRadius(), circle.getCenterY() - circle.getRadius());
        circle_shape.setOutlineThickness(1);
        circle_shape.setOutlineColor(sf::Color(255, 0, 0));

        window.draw(circle_shape);
    }
}

void Window3d::clearTransformations() {
    this->rotates_start = false;
    this->translates_start = false;
    this->resizes_start = false;
    this->sequences_start = false;
    adding_figures = false;
}

void Window3d::redraw() {
    lines.clear();
    bitmap->clearDepth();
    for (auto obj : objects) {
        if (lines_visible) {
            auto object_lines = (*obj).getVisibleLines(*camera);

            for (auto line : object_lines) {
                this->lines.push_back(line);
            }
        }

        z_buffer.processFigure(bitmap, *camera, obj, light_turn_on);
    }
}

void Window3d::makeSequence(const std::string& sequence) {
    if (current_object->sequenceStarted()) {
        return;
    }

    if (sequence == "spinner") {
        TransformationSequence* spinner = new Spinner(*current_x_translate);
        this->current_object->setSequence(spinner);
    } else if (sequence == "carousel") {
        TransformationSequence* carousel = new Carousel(*current_x_translate);
        this->current_object->setSequence(carousel);
    } else if (sequence == "combine") {
        TransformationSequence* combine = new Combine(*current_x_translate);
        this->current_object->setSequence(combine);
    }

    cur_time = 0;
}

void Window3d::finishSequence() {
    current_object->setSequence(nullptr);
    redraw();
}

void Window3d::drawAddingFiguresButtons() {
    if (ImGui::Button("Paint start point Cube")) {
        draw3dObject("cube", "start_point");
    }

    if (ImGui::Button("Paint center mass Cube")) {
        draw3dObject("cube", "center_mass");
    }

    if (ImGui::Button("Paint start point Tetrahedron")) {
        draw3dObject("tetrahedron", "start_point");
    }

    if (ImGui::Button("Paint center mass Tetrahedron")) {
        draw3dObject("tetrahedron", "center_mass");
    }

    if (ImGui::Button("Paint start point Octahedron")) {
        draw3dObject("octahedron", "start_point");
    }

    if (ImGui::Button("Paint start point Icosahedron")) {
        draw3dObject("icosahedron", "start_point");
    }

    if (ImGui::Button("Paint start point Dodecahedron")) {
        draw3dObject("dodecahedron", "start_point");
    }

    if (ImGui::Button("Paint start point Sphere With Pol")) {
        draw3dObject("sphere_with_pol", "start_point");
    }

    if (ImGui::Button("Paint start point Sphere")) {
        draw3dObject("sphere", "start_point");
    }

    if (ImGui::Button("Pint start point Torus")) {
        draw3dObject("torus", "start_point");
    }

    if (ImGui::Button("Pint start point Garlic")) {
        draw3dObject("garlic", "start_point");
    }

    if (ImGui::Button("Pint start point Blob")) {
        draw3dObject("blob", "start_point");
    }

    if (ImGui::Button("Pint start point Ellipse")) {
        draw3dObject("ellipse_3d", "start_point");
    }

    if (ImGui::Button("Pint start point Ellipse V2")) {
        draw3dObject("ellipse_3d_v2", "start_point");
    }

    if (ImGui::Button("Pint start point Fruit")) {
        draw3dObject("fruit", "start_point");
    }

    if (ImGui::Button("Pint start point Moon")) {
        draw3dObject("moon", "start_point");
    }

    if (ImGui::Button("Pint start point Pear")) {
        draw3dObject("pear", "start_point");
    }

    if (ImGui::Button("Pint start point Spring")) {
        draw3dObject("spring", "start_point");
    }

    if (ImGui::Button("Pint start point Spiral")) {
        draw3dObject("spiral", "start_point");
    }

    if (ImGui::Button("Pint start point Equestrian Spiral")) {
        draw3dObject("equestrian_spiral", "start_point");
    }

    for (int i = 0; i < objects.size(); ++i) {
        std::stringstream s;
        s << "Delete object # " << i;
        std::string str;
        s >> str;
        if (ImGui::Button(str.c_str())) {
            deleteObject(i);
        }
    }

    if (ImGui::Button("Go back")) {
        adding_figures = false;
    }
}

void Window3d::drawSequencesButtons() {
    if (ImGui::Button("Start Spinner")) {
        makeSequence("spinner");
    }

    if (ImGui::Button("Start Carousel")) {
        makeSequence("carousel");
    }

    if (ImGui::Button("Start Combine")) {
        makeSequence("combine");
    }

    if (ImGui::Button("Stop")) {
        finishSequence();
    }

    if (ImGui::Button("Go back")) {
        this->sequences_start = false;
    }
}

void Window3d::drawTranslatesButtons() {
    if (ImGui::Button("Translate X >")) {
        this->makeTransformation("TranslateX>");
    }
    if (ImGui::Button("Translate X <")) {
        this->makeTransformation("TranslateX<");
    }

    if (ImGui::Button("Translate Y >")) {
        this->makeTransformation("TranslateY>");
    }
    if (ImGui::Button("Translate Y <")) {
        this->makeTransformation("TranslateY<");
    }

    if (ImGui::Button("Translate Z >")) {
        this->makeTransformation("TranslateZ>");
    }
    if (ImGui::Button("Translate Z <")) {
        this->makeTransformation("TranslateZ<");
    }

    if (ImGui::Button("Go back")) {
        translates_start = false;
    }
}

void Window3d::drawRotatesButtons() {
    if (ImGui::Button("Rotate X >")) {
        this->makeTransformation("RotateX>");
    }
    if (ImGui::Button("Rotate X <")) {
        this->makeTransformation("RotateX<");
    }

    if (ImGui::Button("Rotate Y >")) {
        this->makeTransformation("RotateY>");
    }
    if (ImGui::Button("Rotate Y <")) {
        this->makeTransformation("RotateY<");
    }

    if (ImGui::Button("Rotate Z >")) {
        this->makeTransformation("RotateZ>");
    }
    if (ImGui::Button("Rotate Z <")) {
        this->makeTransformation("RotateZ<");
    }

    if (ImGui::Button("Go back")) {
        translates_start = false;
    }
}

void Window3d::drawResizesButtons() {
    if (ImGui::Button("Resize X >")) {
        this->makeTransformation("ResizeX>");
    }
    if (ImGui::Button("Resize X <")) {
        this->makeTransformation("ResizeX<");
    }

    if (ImGui::Button("Resize Y >")) {
        this->makeTransformation("ResizeY>");
    }
    if (ImGui::Button("Resize Y <")) {
        this->makeTransformation("ResizeY<");
    }

    if (ImGui::Button("Resize Z >")) {
        this->makeTransformation("ResizeZ>");
    }
    if (ImGui::Button("Resize Z <")) {
        this->makeTransformation("ResizeZ<");
    }

    if (ImGui::Button("Resize >")) {
        this->makeTransformation("Resize>");
    }
    if (ImGui::Button("Resize <")) {
        this->makeTransformation("Resize<");
    }

    if (ImGui::Button("Go back")) {
        resizes_start = false;
    }
}

void Window3d::deleteObject(int index) {
    for (int i = index; i < objects.size() - 1; ++i) {
        objects[i] = objects[i + 1];
    }

    objects.pop_back();
}

void Window3d::setColorToFigure(const Color& color) {
    current_object->setColor(new Color(color));
    redraw();
}

void Window3d::drawColorButtons() {
    if (ImGui::Button("color red")) {
        this->setColorToFigure(Color::getRed());
    }

    if (ImGui::Button("color black")) {
        this->setColorToFigure(Color::getBlack());
    }

    if (ImGui::Button("color blue")) {
        this->setColorToFigure(Color::getBlue());
    }

    if (ImGui::Button("color orange")) {
        this->setColorToFigure(Color::getOrange());
    }

    if (ImGui::Button("color green")) {
        this->setColorToFigure(Color::getGreen());
    }

    if (ImGui::Button("Go back")) {
        set_colors_start = false;
    }
}
