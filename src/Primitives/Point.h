#ifndef PC_GRAPH_POINT_H
#define PC_GRAPH_POINT_H

#include <cmath>

#include <SFML/System/Vector2.hpp>
#include "../Math/Matrix.hpp"

class Point {
private:
    constexpr static const double eps = 0.0000001;

public:
    Point();

    Point(double x, double y);

    Point(int x, int y);

    Point(const Matrix<double>& matrix);

    double getX() const;

    double getY() const;

    int getIntX() const;

    int getIntY() const;

    sf::Vector2f getVector2f() const;

    double diff(const Point& point);

    static Point generateRandomPoint(int max_x, int max_y);

    void decX();

    void incX();

    void setX(double x);

    void setY(double y);

    Point shift(double x, double y);

    friend bool operator==(const Point& lhs, const Point& rhs);

private:
    double x, y;
};


#endif //PC_GRAPH_POINT_H
