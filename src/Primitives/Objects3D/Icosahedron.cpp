#include "Icosahedron.h"

std::vector<Line> Icosahedron::getLines(const Camera &camera) const {
    auto points = getPoints(camera);

    return {
            { points[0], points[1] },
            { points[0], points[2] },
            { points[1], points[2] },

            { points[3], points[2] },
            { points[3], points[1] },

            { points[3], points[4] },
            { points[1], points[4] },

            { points[3], points[5] },
            { points[4], points[5] },

            { points[5], points[6] },
            { points[4], points[6] },

            { points[5], points[7] },
            { points[6], points[7] },

            { points[6], points[8] },
            { points[7], points[8] },

            { points[7], points[9] },
            { points[8], points[9] },

            { points[9], points[0] },
            { points[8], points[0] },

            { points[2], points[9] },

            { points[2], points[10] },
            { points[9], points[10] },

            { points[3], points[10] },

            { points[5], points[10] },

            { points[7], points[10] },

            { points[8], points[11] },
            { points[6], points[11] },

            { points[0], points[11] },

            { points[1], points[11] },

            { points[4], points[11] },
            { points[1], points[2] },

            { points[1], points[2] },
            { points[1], points[2] },
            { points[1], points[2] },
    };
}

Icosahedron::Icosahedron(const Point3D &point) {
    this->start_point = point;

    double k = (1 + sqrt(5)) / 2;
    this->points = {
            { -size, 0, k * size },
            { size, 0, k * size },
            { 0, k * size, size },
            { k * size, size, 0 },
            { k * size, -size, 0 },
            { size, 0, -k * size },
            { 0, -k * size, -size },
            { -size, 0, -k * size },
            { -k * size, -size, 0 },
            { -k * size, size, 0 },
            { 0, k * size, -size },
            { 0, -k * size, size },
    };
}

std::vector<Face> Icosahedron::getFaces(const Camera &camera) const {
    auto points = getPointsWithoutProjection(camera);

    return {
        { points[0], points[1], points[2] },
        { points[3], points[2], points[1] },
        { points[3], points[1], points[4] },
        { points[3], points[4], points[5] },
        { points[5], points[4], points[6] },
        { points[5], points[6], points[7] },
        { points[6], points[8], points[7] },
        { points[7], points[8], points[9] },
        { points[9], points[8], points[0] },
        { points[9], points[0], points[2] },
        { points[2], points[10], points[9] },
        { points[2], points[3], points[10] },
        { points[3], points[5], points[10] },
        { points[5], points[7], points[10] },
        { points[7], points[9], points[10] },
        { points[8], points[6], points[11] },
        { points[8], points[11], points[0] },
        { points[1], points[0], points[11] },
        { points[1], points[11], points[4] },
        { points[4], points[11], points[6] }
    };
}
