#include "Combine.h"

std::vector<TransformationInterface*> Combine::iterate(const Matrix<double> &input) {
    std::vector<TransformationInterface*> result;

    TransformationInterface* translate = new Translate;

    (dynamic_cast<Translate&>(*translate)).setCoordinates({ shift_distance * cos(cur_angle + rotate_angle), 0, shift_distance * sin(cur_angle + rotate_angle) });
    result.push_back(translate);

    TransformationInterface* rotate = new RotateY();

    (dynamic_cast<RotateY&>(*rotate)).setAngle(cur_angle + rotate_angle);
    result.push_back(rotate);

    return result;
}

void Combine::clear() {
    cur_angle = 0;
}

void Combine::nextTick() {
    cur_angle += 2 * rotate_angle;
}

Combine::Combine(double shift_distance) {
    this->shift_distance = shift_distance;
}
