#ifndef MYPROJECT_ROTATEY_H
#define MYPROJECT_ROTATEY_H

#include "Rotate.h"

#include <cmath>

class RotateY : public Rotate {
public:
    Matrix<double> process(Matrix<double> value) const override;
};


#endif //MYPROJECT_ROTATEY_H
