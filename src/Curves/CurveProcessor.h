#ifndef MYPROJECT_CURVEPROCESSOR_H
#define MYPROJECT_CURVEPROCESSOR_H

#include "../Primitives/Point.h"

#include "CurveProcessorInterface.h"

#include "Bezier/BezierProcessor.h"
#include "BSplain/BSplainProcessor.h"

#include <string>
#include <vector>
#include <unordered_map>

class CurveProcessor : public CurveProcessorInterface {
private:
    CurveProcessorInterface* processor;
    std::unordered_map<std::string, CurveProcessorInterface*> processor_mapper;

public:
    CurveProcessor();

    std::vector<Point> getCurve(const std::vector<Point>& points) override;

    std::vector<Point> getCloseCurve(const std::vector<Point>& points) override;

    void setAlgo(const std::string& algo) override;

    void setProcessor(const std::string& processor);
};

#endif //MYPROJECT_CURVEPROCESSOR_H
