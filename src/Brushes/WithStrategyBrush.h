#ifndef MYPROJECT_WITHSTRATEGYBRUSH_H
#define MYPROJECT_WITHSTRATEGYBRUSH_H

#include "AbstractBrush.h"
#include "StrategyInterface.h"

class WithStrategyBrush : public AbstractBrush {
protected:
    StrategyInterface* strategy = nullptr;

public:
    bool withStrategy() override;

    void setStrategy(StrategyInterface* const strategy);

    WithStrategyBrush(Bitmap* const bitmap);
};


#endif //MYPROJECT_WITHSTRATEGYBRUSH_H
