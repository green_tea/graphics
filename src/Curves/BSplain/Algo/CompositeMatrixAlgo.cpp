#include "CompositeMatrixAlgo.h"

std::vector<Point> CompositeMatrixAlgo::createCurve(const std::vector<Point> &points) {
    size_t chunk_size = 4;

    std::vector<Point> result;
    std::vector<Point> cur_chunk;

    for (double t = 0; t <= 1; t += this->step) {
        for (int i = 0; i < points.size(); ++i) {
            cur_chunk.emplace_back(points[i]);

            if (cur_chunk.size() == chunk_size) {
                MatrixAlgo::prepareMatrices(cur_chunk);
//                Point point = MatrixAlgo::processCurveStep(cur_chunk, t - i + 3);
                Point point = MatrixAlgo::processCurveStep(cur_chunk, t);

                result.emplace_back(point);

                prepareChunk(cur_chunk);
            }
        }

        cur_chunk.clear();
    }

    return result;
}


void CompositeMatrixAlgo::prepareChunk(std::vector<Point> &chunk) {
    swap(chunk[0], chunk[1]);
    swap(chunk[1], chunk[2]);
    swap(chunk[2], chunk[3]);
    swap(chunk[3], chunk[4]);
    chunk.pop_back();
}

void CompositeMatrixAlgo::swap(Point &p1, Point &p2) {
    Point tmp = p1;
    p1 = p2;
    p2 = tmp;
}
