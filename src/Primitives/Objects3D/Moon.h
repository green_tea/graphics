#ifndef MYPROJECT_MOON_H
#define MYPROJECT_MOON_H

#include "SphereWithPolInterface.h"

class Moon : public SphereWithPolInterface {
public:
    explicit Moon(const Point3D& start_point, double radius, int latitudes_count, int longitudes_count);
};


#endif //MYPROJECT_MOON_H
