#ifndef MYPROJECT_PEARSTARTPOINTBUILDER_H
#define MYPROJECT_PEARSTARTPOINTBUILDER_H

#include "../BuilderInterface.h"
#include "../../Pear.h"

class PearStartPointBuilder : public BuilderInterface {
public:
    void setStartPoint(const Point3D& point) override;
};


#endif //MYPROJECT_PEARSTARTPOINTBUILDER_H
