#include "Point.h"

Point::Point(double x, double y) : x(x), y(y) {}

double Point::getX() const {
    return x;
}

double Point::getY() const {
    return y;
}

int Point::getIntX() const {
    return static_cast<int>(std::round(x));
}

Point::Point() : x(0), y(0) {}

int Point::getIntY() const {
    return static_cast<int>(std::round(y));
}

sf::Vector2f Point::getVector2f() const {
    return {x, y};
}

double Point::diff(const Point& point) {
    return std::sqrt((point.x - x) * (point.x - x) + (point.y - y) * (point.y - y));
}

Point::Point(int x, int y) : x(static_cast<double>(x)), y(static_cast<double>(y)) {}

Point Point::generateRandomPoint(int max_x, int max_y) {
    return {(rand() % max_x), (rand() % max_y)};
}

void Point::decX() {
    x -= 1;
}

void Point::incX() {
    x += 1;
}

void Point::setX(double x) {
    this->x = x;
}

void Point::setY(double y) {
    this->y = y;
}

Point Point::shift(double x, double y) {
    this->x += x;
    this->y += y;
}

Point::Point(const Matrix<double>& matrix) {
    double h = matrix.get(0, 3);
    this->x = matrix.get(0, 0) / h;
    this->y = matrix.get(0, 1) / h;
}

bool operator==(const Point &lhs, const Point &rhs) {
    return fabs(rhs.getX() - lhs.getX()) < Point::eps && fabs(rhs.getY() - lhs.getY()) < Point::eps;
}
