#ifndef MYPROJECT_COUNTDOWNSERVICE_H
#define MYPROJECT_COUNTDOWNSERVICE_H

#include "CountDownServiceInterface.h"

class CountDownService : public CountDownServiceInterface {
private:
    CountDownService() = default;

    static CountDownService* instance;

public:
    int getCode(const Rectangle &rectangle, const Point &point) const override;

    void shiftToLine(Line &line1, const Line &line2, bool is_first) const override;

    int getNearestLine(const Rectangle& rectangle, const Point& point) const override;

    static CountDownService* getInstance();
};



#endif //MYPROJECT_COUNTDOWNSERVICE_H
