#include "CurveProcessor.h"

void CurveProcessor::setProcessor(const std::string &processor) {
    this->processor = processor_mapper[processor];
}

CurveProcessor::CurveProcessor() {
    this->processor_mapper["bezier"] = new BezierProcessor();
    this->processor_mapper["bsplain"] = new BSplainProcessor();

    this->processor = this->processor_mapper["bezier"];
}

std::vector<Point> CurveProcessor::getCurve(const std::vector<Point> &points) {
    return this->processor->getCurve(points);
}

std::vector<Point> CurveProcessor::getCloseCurve(const std::vector<Point> &points) {
    return this->processor->getCloseCurve(points);
}

void CurveProcessor::setAlgo(const std::string &algo) {
    processor->setAlgo(algo);
}
