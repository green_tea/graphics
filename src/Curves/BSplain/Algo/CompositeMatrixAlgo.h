#ifndef MYPROJECT_COMPOSITEMATRIXALGO_H
#define MYPROJECT_COMPOSITEMATRIXALGO_H

#include "MatrixAlgo.h"

class CompositeMatrixAlgo : public MatrixAlgo {
private:
    void prepareChunk(std::vector<Point>& chunk);

    void swap(Point& p1, Point& p2);
public:
    std::vector<Point> createCurve(const std::vector<Point>& points) override;
};


#endif //MYPROJECT_COMPOSITEMATRIXALGO_H
