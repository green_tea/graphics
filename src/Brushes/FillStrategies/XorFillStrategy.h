#ifndef MYPROJECT_XORFILLSTRATEGY_H
#define MYPROJECT_XORFILLSTRATEGY_H

#include "../LineStrategyInterface.h"

#include "../../Primitives/Line.h"

#include <vector>

class XorFillStrategy : public LineStrategyInterface {
public:
    void operator()(const Point& point, Bitmap *bitmap, const std::vector<Line>& lines, const Color &color) override;

};


#endif //MYPROJECT_XORFILLSTRATEGY_H
