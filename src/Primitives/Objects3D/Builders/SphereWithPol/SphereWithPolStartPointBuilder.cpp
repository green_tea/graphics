#include "SphereWithPolStartPointBuilder.h"

void SphereWithPolStartPointBuilder::setStartPoint(const Point3D &point) {
    this->object = new SphereWithPol(point, SphereWithPol::size, 36, 18);
}
