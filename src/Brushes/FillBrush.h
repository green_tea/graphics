#ifndef MYPROJECT_FILLBRUSH_H
#define MYPROJECT_FILLBRUSH_H

#include "AbstractBrush.h"
#include "WithStrategyBrush.h"

#include "../Primitives/Point.h"

#include "../Structures/Bitmap.h"

class FillBrush : public WithStrategyBrush {

public:
    FillBrush(Bitmap* const bitmap);

    void applyToPixel(const Point& point) override;
};


#endif //MYPROJECT_FILLBRUSH_H
