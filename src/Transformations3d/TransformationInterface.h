#ifndef MYPROJECT_TRANSFORMATIONINTERFACE_H
#define MYPROJECT_TRANSFORMATIONINTERFACE_H

#include "../Math/Matrix.hpp"

class TransformationInterface {
public:
    virtual Matrix<double> process(Matrix<double> value) const = 0;
};

#endif //MYPROJECT_TRANSFORMATIONINTERFACE_H
