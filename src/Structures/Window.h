#ifndef MYPROJECT_WINDOW_H
#define MYPROJECT_WINDOW_H

#include <functional>
#include <cmath>
#include <iostream>
#include <string>

#include <imgui-SFML.h>
#include <imgui.h>

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/CircleShape.hpp>

#include <spdlog/spdlog.h>

#include "../Primitives/Point.h"
#include "../Primitives/Line.h"
#include "../Primitives/Circle.h"
#include "../Primitives/Ellipse.h"
#include "../Primitives/EllipseShape.h"

#include "../Brushes/AbstractBrush.h"
#include "../Brushes/StrategyInterface.h"
#include "../Brushes/FillBrush.h"
#include "../Brushes/FillStrategies/DefaultFillStrategy.h"
#include "../Brushes/FillStrategies/StringStrategy.h"
#include "../Brushes/FillStrategies/XorFillStrategy.h"
#include "../Brushes/LineFillBrush.h"

#include "../Tools/CountDown.h"

#include "../Curves/CurveProcessor.h"
#include "../Curves/CurveAlgoInterface.h"

#include "Color.h"
#include "Bitmap.h"

class Window {
public:
    static const int WIDTH = 1920;
    static const int HEIGHT = 1080;

    static const int FRAME_LIMIT = 60;

    void setPixel(const Color& color, const Point& point);

    void clearPixels();

    CountDown countDown;

    sf::RenderWindow window;
    sf::Texture texture;
    sf::Sprite sprite;

    explicit Window() : window(sf::VideoMode(WIDTH, HEIGHT), "SFML") {
        srand(time(0));
        this->window.setFramerateLimit(FRAME_LIMIT);

        ImGui::SFML::Init(window);

        texture.create(WIDTH, HEIGHT);
        this->sprite = sf::Sprite(texture);

        this->bitmap = new Bitmap(Window::WIDTH, Window::HEIGHT);
        this->generateBrushes();

        this->curve_processor = new CurveProcessor();
    };

    sf::RenderWindow& getWindow();

    void rightMousePress();

    void rightMousePull();

    void drawPoints(const std::vector<Point>& points, const Color& color = Color::getRed());

    void drawPanel();

    void update(sf::Time time);

    void processEvent(sf::Event event);

private:
    AbstractBrush** brushes;
    StrategyInterface** fill_strategies;
    LineStrategyInterface** line_fill_strategies;

    CurveProcessor* curve_processor;

    Bitmap* bitmap;

    std::vector<sf::Vertex*> lines;
    std::vector<Line> prepared_lines;
    std::vector<Circle> circles;
    std::vector<Ellipse> ellipses;
    Rectangle* count_down_rect = nullptr;

    bool right_mouse_pressed = false;
    bool count_down_started = false;
    bool try_to_set_count_down_rectangle = false;
    bool curve_started = false;
    bool line_fill_started = false;
    bool start_xor_fill = false;
    bool finished_xor_fill = false;

    Point* last_point = nullptr;
    Point cur_start_point;

    std::string curve_processor_string = "bezier";

    Point start_mouse_pressed_point;
    Point end_mouse_pressed_point;

    int selected_item = 1;
    int selected_type = 0;
    int selected_brush = 0;
    int selected_brush_strategy = 0;
    int selected_color = 0;
    int selected_count_down_strategy = 0;

    const char* items[10]{ "Brush", "Bresenham Line", "SFML Line", "Both Line", "Bresenham Circle", "SFML Circle", "Both Circle", "Bresenham Ellipse", "SFML Ellipse", "Both Ellipse"};
    const char* drawing_types[2]{"Manual", "Random"};
    const char* brush_labels[2]{"Fill", "LINE FILL"};
    const char* fill_strategy_labels[2]{ "Default", "String" };
    const char* color_labels[7]{"Red", "Blue", "Green", "Black", "White", "Yellow", "Orange"};
    const char* count_down_types[2]{ "With types", "Full" };

    std::vector<Point> curve_points;
    std::vector<Point> draw_curve_points;

    void drawLines();

    void drawCircles();

    void drawEllipses();

    void showFileInfo();

    void uploadFile();

    void drawBresenhamLine(const Point& p1, const Point& p2);

    void drawSimpleLine(const Point& p1, const Point& p2, const Color& color = Color::getRed());

    void drawSimpleRectangle(const Rectangle& rectangle, const Color& color);

    void drawBresenhamCircle(const Point& p1, const Point& p2);

    void drawSimpleCircle(const Point& p1, const Point& p2);

    void drawBresenhamEllipse(const Point& p1, const Point& p2);

    void drawSimpleEllipse(const Point& p1, const Point& p2);

    void generateBrushes();

    bool brushChecked();

    void useCurrentBrush(const Point& point);

    void closeInFill();

    StrategyInterface* getCurStrategy();

    LineStrategyInterface* getCurLineStrategy();

    Point getCurPoint();

    void setCountDownType(int type);

    void drawCountDown();

    void addLineToCountDown(const Line& line);

    void setCountDownRectangle(const Rectangle& rectangle);

    void clearCountDown();

    void drawCloseCurve(const std::string& algo);

    void drawCurve(const std::string& algo);

    void clear();

    void drawPointsWithoutSave(const std::vector<Point>& points, const Color& color = Color::getRed());
};


#endif //MYPROJECT_WINDOW_H
