#ifndef MYPROJECT_ELLIPSE3DSTARTPOINTBUILDER_H
#define MYPROJECT_ELLIPSE3DSTARTPOINTBUILDER_H

#include "../BuilderInterface.h"
#include "../../Ellipse3D.h"

class Ellipse3DStartPointBuilder : public BuilderInterface {
public:
    void setStartPoint(const Point3D& point) override;
};


#endif //MYPROJECT_ELLIPSE3DSTARTPOINTBUILDER_H
