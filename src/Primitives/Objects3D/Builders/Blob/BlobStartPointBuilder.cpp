#include "BlobStartPointBuilder.h"

void BlobStartPointBuilder::setStartPoint(const Point3D &point) {
    this->object = new Blob(point, Blob::size, 36, 18);
}
