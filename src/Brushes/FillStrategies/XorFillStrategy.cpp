#include "XorFillStrategy.h"

void XorFillStrategy::operator()(const Point& point, Bitmap *bitmap, const std::vector<Line>& lines, const Color &color) {
    this->start_color = bitmap->getPixel(point);

    if (lines.empty()) {
        return;
    }

    for (int i = 0; i < lines.size(); ++i) {
        Line line = lines[i];
        line.prepareCoordinates(false);

        Point start = line.getStartPoint();
        Point finish = line.getFinishPoint();

        for (int y = start.getIntY(); y < finish.getIntY(); ++y) {
            int x = start.getIntX() + (y - start.getIntY()) * (finish.getIntX() - start.getIntX()) / (finish.getIntY() - start.getIntY());
            for (int i = x + 1; i < bitmap->getWidth(); ++i) {
                Point cur_point(i, y);
                Color pixel_color = bitmap->getPixel(cur_point);

                if (this->start_color == pixel_color) {
                    bitmap->setPixel(cur_point, color);
                } else if (pixel_color == color) {
                    bitmap->setPixel(cur_point, this->start_color);
                }
            }
        }
    }

}
