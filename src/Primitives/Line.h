#ifndef MYPROJECT_LINE_H
#define MYPROJECT_LINE_H

#include <vector>
#include <tuple>

#include "Point.h"

class Line {
private:
    Point start_point;
    Point finish_point;

public:
    Line(Point start_point, Point finish_point);

    Line(double x1, double y1, double x2, double y2);

    std::vector<Point> getBresenham();

    Point getStartPoint() const;

    Point getFinishPoint() const;

    void setStartPoint(const Point& point);

    void setFinishPoint(const Point& point);

    std::tuple<double, double, double> getEquation() const;

    Point intersect(const Line& line);

    Point getPoint(double percent) const;

    void prepareCoordinates(bool is_x);
};


#endif //MYPROJECT_LINE_H
