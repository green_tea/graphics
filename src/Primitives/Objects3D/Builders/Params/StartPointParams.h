#ifndef MYPROJECT_STARTPOINTPARAMS_H
#define MYPROJECT_STARTPOINTPARAMS_H

class StartPointParams : public ParamsInterface {
private:
    Point3D start_point;

public:
    Point3D getStartPoint() const override {
        return start_point;
    }

    explicit StartPointParams(Point3D start_point) {
        this->start_point = start_point;
    };
};

#endif //MYPROJECT_STARTPOINTPARAMS_H
