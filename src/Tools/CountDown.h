#ifndef MYPROJECT_COUNTDOWN_H
#define MYPROJECT_COUNTDOWN_H

#include <vector>
#include <utility>

#include "../Primitives/Line.h"
#include "../Primitives/Rectangle.h"

#include "../Structures/Color.h"

#include "CountDownStrategies/StrategyManager.h"
#include "Classes.h"

class CountDown {
private:
    std::vector<Line> lines;
    Rectangle rectangle;
    CountDownStrategyInterface* strategy;

public:
    CountDown();

    CountDown(CountDownStrategyInterface* strategy);

    CountDown(std::vector<Line> lines, Rectangle rectangle, CountDownStrategyInterface* strategy);

    void addLine(const Line& line);

    void setRectangle(const Rectangle& rectangle);

    std::vector<Line> getLines() const;

    Rectangle getRectangle() const;

    std::pair<Rectangle, std::vector<std::pair<Line, Color> > > getResult() const;

    void setStrategy(CountDownStrategyInterface* strategy);

    void clear();
};


#endif //MYPROJECT_COUNTDOWN_H
