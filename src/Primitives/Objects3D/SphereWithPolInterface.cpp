#include "SphereWithPolInterface.h"

std::vector<Line> SphereWithPolInterface::getLines(const Camera &camera) const {
    return {};
}

std::vector<Face> SphereWithPolInterface::getFaces(const Camera &camera) const {
    auto points = getPointsWithoutProjection(camera);
    std::vector<Face> faces;

    for (int i = 0; i + latitudes_count + 2 < points.size(); ++i) {
        faces.emplace_back(points[i], points[i + 1], points[i + latitudes_count + 1]);
        faces.emplace_back(points[i + 1], points[i + latitudes_count + 1], points[i + latitudes_count + 2]);
    }

    return faces;
}

SphereWithPolInterface::SphereWithPolInterface(const Point3D &start_point, std::function<Point3D(double i, double j)> func,
                                               double radius, int latitudes_count, int longitudes_count) {
    this->latitudes_count = latitudes_count;
    this->radius = radius;

    double latitude_step = 360.0 / latitudes_count;
    double longitude_step = 360.0 / longitudes_count;

    for (double i = 0; i <= 360; i += longitude_step) {
        for (double j = 0; j <= 360; j += latitude_step) {
            points.push_back(func(i, j));
        }
    }

    this->start_point = start_point;
}
