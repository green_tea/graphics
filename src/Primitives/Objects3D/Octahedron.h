#ifndef MYPROJECT_OCTAHEDRON_H
#define MYPROJECT_OCTAHEDRON_H

#include "Object3DInterface.h"

class Octahedron : public Object3DInterface {
public:
    std::vector<Line> getLines(const Camera& camera) const override;

    std::vector<Face> getFaces(const Camera& camera) const override;

    explicit Octahedron(const Point3D& point);
};


#endif //MYPROJECT_OCTAHEDRON_H
