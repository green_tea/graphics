#include "Resize.h"

Matrix<double> Resize::process(Matrix<double> value) const {
    Matrix<double> rotation_matrix = Matrix<double>(4, 4, {{
       {point.getX(), 0, 0, 0},
       {0, point.getY(), 0, 0},
       {0, 0, point.getZ(), 0},
       {0, 0, 0, 1},
    }});

    return value * rotation_matrix;
}

void Resize::setCoordinates(const Point3D& point) {
    this->point = point;
}

Point3D Resize::getCoordinates() const {
    return point;
}