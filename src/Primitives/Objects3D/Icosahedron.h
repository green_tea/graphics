#ifndef MYPROJECT_ICOSAEDRON_H
#define MYPROJECT_ICOSAEDRON_H

#include "Object3DInterface.h"

class Icosahedron : public Object3DInterface {
public:
    std::vector<Line> getLines(const Camera& camera) const override;

    std::vector<Face> getFaces(const Camera& camera) const override;

    explicit Icosahedron(const Point3D& point);
};


#endif //MYPROJECT_ICOSAEDRON_H
