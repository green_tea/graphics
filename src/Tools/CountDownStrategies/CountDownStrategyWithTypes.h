#ifndef MYPROJECT_COUNTDOWNSTRATEGYWITHTYPES_H
#define MYPROJECT_COUNTDOWNSTRATEGYWITHTYPES_H

#include "CountDownStrategyInterface.h"

#include "../CountDown.h"

#include "../../Primitives/Line.h"

class CountDownStrategyWithTypes : public CountDownStrategyInterface {
    std::pair<Rectangle, std::vector<std::pair<Line, Color>>> getResult(const CountDown *count_down) override;
};


#endif //MYPROJECT_COUNTDOWNSTRATEGYWITHTYPES_H
