#ifndef MYPROJECT_PEAR_H
#define MYPROJECT_PEAR_H

#include "SphereWithPolInterface.h"

class Pear : public SphereWithPolInterface {
public:
    explicit Pear(const Point3D& start_point, double radius, int latitudes_count, int longitudes_count);
};


#endif //MYPROJECT_PEAR_H
