#include "EquestrianSpiralStartPointBuilder.h"

void EquestrianSpiralStartPointBuilder::setStartPoint(const Point3D &point) {
    this->object = new EquestrianSpiral(point, EquestrianSpiral::size / 2, EquestrianSpiral::size, 32, 18);
}
