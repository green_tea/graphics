#ifndef MYPROJECT_ELLIPSE3DV2STARTPOINTBUILDER_H
#define MYPROJECT_ELLIPSE3DV2STARTPOINTBUILDER_H

#include "../BuilderInterface.h"
#include "../../Ellipse3DV2.h"

class Ellipse3DV2StartPointBuilder : public BuilderInterface {
public:
    void setStartPoint(const Point3D& point) override;
};


#endif //MYPROJECT_ELLIPSE3DV2STARTPOINTBUILDER_H
