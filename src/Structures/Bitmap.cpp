#include "Bitmap.h"

Bitmap::Bitmap(int width, int height) : width(width), height(height) {
    this->pixels = new sf::Uint8[width * height * 4];
    this->depth = new double[width * height];

    clearDepth();
}

void Bitmap::setPixel(const Point& point, const Color &color) {
    this->setIndex(getIndexByPoint(point), color);
}

Color Bitmap::getPixel(const Point &point) const {
    int index = getIndexByPoint(point);
    return {this->pixels[index], this->pixels[index + 1], this->pixels[index + 2], this->pixels[index + 3]};
}

void Bitmap::setIndex(int index, const Color& color) {
    this->pixels[index] = color.getR();
    this->pixels[index + 1] = color.getG();
    this->pixels[index + 2] = color.getB();
    this->pixels[index + 3] = color.getA();
}

int Bitmap::getIndexByPoint(const Point& point) const {
    return (point.getIntY() * this->width + point.getIntX()) * 4;
}

sf::Uint8 *Bitmap::getPixels() {
    return this->pixels;
}

bool Bitmap::isInside(const Point& point) const {
    return isInsideDoubles(point.getX(), point.getY());
}

Bitmap::Bitmap() : pixels(nullptr), width(0), height(0) {}

int Bitmap::getWidth() {
    return this->width;
}

double Bitmap::getDepth(int x, int y) const {
    return depth[getDepthIndexByPoint({ x, y })];
}

int Bitmap::getHeight() {
    return this->height;
}

void Bitmap::setDepth(int x, int y, double value) {
    setDepthByIndex(getDepthIndexByPoint({ x, y }), value);
}

void Bitmap::setDepthByIndex(int index, double value) {
    depth[index] = value;
}

int Bitmap::getDepthIndexByPoint(const Point &point) const {
    return (point.getIntY() * this->width + point.getIntX());
}

void Bitmap::clearDepth() {
    std::fill(pixels, pixels + width * height * 4, 255);
    std::fill(depth, depth + width * height, -10000);
}

bool Bitmap::isInsideDoubles(double x, double y) const {
    return x >= 0 && x < this->width && y >= 0 && y < this->height;
}
