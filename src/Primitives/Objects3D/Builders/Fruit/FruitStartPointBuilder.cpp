#include "FruitStartPointBuilder.h"

void FruitStartPointBuilder::setStartPoint(const Point3D &point) {
    this->object = new Fruit(point, Fruit::size, 36, 18);
}
