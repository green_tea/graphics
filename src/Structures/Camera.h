#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <limits>
#include <vector>

#include "../Math/Matrix.hpp"
#include "../Transformations3d/TransformationProcessor.hpp"

#include "../Transformations3d/Translate.h"
#include "../Transformations3d/Resize.h"

#include "../Primitives/Face.h"

#include <iostream>

class Camera {
public:
    Camera() = delete;

    explicit Camera(unsigned long long w, unsigned long long h)
            : width(w), height(h) {}


    Matrix<double> makeProjection(Matrix<double> v) const;

    Matrix<double> toScreen(Matrix<double> v) const;

    double getIntensity(const Face& face, const Point3D& point) const;

    bool perspectiveEnabled = false;
    double perspectiveCoefficient = 500;

private:
    unsigned long long width;
    unsigned long long height;

    TransformationProcessor transformation_processor;

    // интенсивность рассеянного света
    double IA = 1;
    // коэффициент диффузного отражения
    double KA = 0.1;
    // Коэффициент точечного источника
    double IL = 750;
    // Коэффициент диффузного отражения
    double KD = 0.8;
    // Произвольная константа
    double K = 0.5;
    // Константа, которая определяется экспериментально
    double KS = 0.1;
    // степень, аппроксимирующая пространственное распределение зеркально отраженного света
    int M = 10;
};

#endif  // CAMERA_HPP
