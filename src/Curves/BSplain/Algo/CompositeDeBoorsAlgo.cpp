#include "CompositeDeBoorsAlgo.h"

std::vector<Point> CompositeDeBoorsAlgo::createCurve(const std::vector<Point> &points) {
    size_t chunk_size = 4;

    std::vector<Point> result;
    std::vector<Point> cur_chunk;

    size_t cur_step = 0;
    for (double t = 0; t <= 1; t += this->step) {
        for (int i = 0; i < points.size(); ++i) {
            cur_chunk.emplace_back(points[i]);

            if (cur_chunk.size() == chunk_size) {
//                Point point = MatrixAlgo::processCurveStep(cur_chunk, t - i + 3);
                Point point = DeBoorsAlgo::getNextPoint(cur_chunk, t, points.size(), cur_step);

                result.emplace_back(point);

                prepareChunk(cur_chunk);
            }
        }

        cur_chunk.clear();
        ++cur_step;
    }

    return result;
}

void CompositeDeBoorsAlgo::prepareChunk(std::vector<Point> &chunk) {
    swap(chunk[0], chunk[1]);
    swap(chunk[1], chunk[2]);
    swap(chunk[2], chunk[3]);
    swap(chunk[3], chunk[4]);
    chunk.pop_back();
}

void CompositeDeBoorsAlgo::swap(Point &p1, Point &p2) {
    Point tmp = p1;
    p1 = p2;
    p2 = tmp;
}
