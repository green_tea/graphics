#ifndef MYPROJECT_ZBUFFER_H
#define MYPROJECT_ZBUFFER_H

#include "Bitmap.h"
#include "Color.h"
#include "../Primitives/Objects3D/Object3DInterface.h"

#include <iostream>

class ZBuffer {
private:
    constexpr static const double EPS = 0.0000001;

    double step = 0.1;

private:

    std::pair<double, double> getSteps(const Point3D& p1, const Point3D& p2) const;

    bool isEqual(double a, double b);

    void drawFace(Bitmap* bitmap, Face face, const Color& color, const Camera& camera, bool with_intensity, const Point3D& center_point);

    void fillTopTriangle(Bitmap* bitmap, Face face, const Color& color, const Camera& camera, bool with_intensity);

    void fillBottomTriangle(Bitmap* bitmap, Face face, const Color& color, const Camera& camera, bool with_intensity);

    Point3D generateSplitPoint(const Point3D& p1, const Point3D& p2, const Point3D& p3);

public:
    void processFigure(Bitmap* bitmap, const Camera& camera, Object3DInterface* object, bool with_intensity);
};


#endif //MYPROJECT_ZBUFFER_H
