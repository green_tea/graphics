#include "Octahedron.h"

Octahedron::Octahedron(const Point3D &point) {
    this->start_point = point.shift({ 0, -size / 2, -size / 2 });
    this->points = {
            { size / 2, size / 2, 0 },
            { size / 2, 0, size / 2 },
            { 0, size / 2, size / 2 },
            { size / 2, size / 2, size },
            { size / 2, size, size / 2 },
            { size, size / 2, size / 2 },
    };
}

std::vector<Line> Octahedron::getLines(const Camera &camera) const {
    auto points = getPoints(camera);

    return {
            { points[0], points[2] },
            { points[0], points[4] },
            { points[0], points[5] },
            { points[0], points[1] },

            { points[3], points[2] },
            { points[3], points[4] },
            { points[3], points[5] },
            { points[3], points[1] },

            { points[2], points[4] },
            { points[2], points[1] },

            { points[5], points[4] },
            { points[5], points[1] },
    };
}

std::vector<Face> Octahedron::getFaces(const Camera &camera) const {
    auto points = getPointsWithoutProjection(camera);

    return {
        { points[0], points[2], points[3] },
        { points[0], points[3], points[4] },
        { points[0], points[4], points[5] },
        { points[0], points[5], points[2] },
        { points[1], points[3], points[2] },
        { points[1], points[4], points[3] },
        { points[1], points[5], points[4] },
        { points[1], points[2], points[5] }
    };
}
