#include "LineStrategyBrush.h"

bool LineStrategyBrush::withStrategy() {
    return true;
}

void LineStrategyBrush::setStrategy(LineStrategyInterface *const strategy) {
    this->strategy = strategy;
}

LineStrategyBrush::LineStrategyBrush(Bitmap *bitmap) : AbstractBrush(bitmap) {}