#include "FillBrush.h"

void FillBrush::applyToPixel(const Point &point) {
    this->strategy->operator()(point, AbstractBrush::bitmap, AbstractBrush::color);
}

FillBrush::FillBrush(Bitmap* const bitmap) : WithStrategyBrush(bitmap) {}
