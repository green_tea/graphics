#include "AbstractBrush.h"

AbstractBrush::AbstractBrush(Bitmap *bitmap) {
    this->bitmap = bitmap;
}

void AbstractBrush::setColor(const Color &color) {
    this->color = color;
}

bool AbstractBrush::withStrategy() {
    return false;
}

void AbstractBrush::setLines(std::vector<Line> lines) {
    this->lines = std::move(lines);
}
