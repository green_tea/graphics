#ifndef MYPROJECT_CURVEALGOINTERFACE_H
#define MYPROJECT_CURVEALGOINTERFACE_H

#include "../Primitives/Point.h"

#include <vector>

class CurveAlgoInterface {
public:
    virtual std::vector<Point> createCurve(const std::vector<Point>& points) = 0;
};

#endif //MYPROJECT_CURVEALGOINTERFACE_H
