#ifndef MYPROJECT_STRATEGYINTERFACE_H
#define MYPROJECT_STRATEGYINTERFACE_H

#include "../Primitives/Point.h"
#include "../Structures/Bitmap.h"

class StrategyInterface {
protected:
    Bitmap* bitmap = nullptr;
    Color color;
    Color start_color;

public:
    virtual void operator()(const Point& point, Bitmap* bitmap, const Color& color) = 0;
};


#endif //MYPROJECT_STRATEGYINTERFACE_H
