#include "MoonStartPointBuilder.h"

void MoonStartPointBuilder::setStartPoint(const Point3D &point) {
    this->object = new Moon(point, Moon::size, 36, 18);
}
