#include "LineFillBrush.h"

void LineFillBrush::applyToPixel(const Point &point) {
    this->strategy->operator()(point, AbstractBrush::bitmap, AbstractBrush::lines, AbstractBrush::color);
}

LineFillBrush::LineFillBrush(Bitmap* const bitmap) : LineStrategyBrush(bitmap) {}