#include "DeBoorsAlgo.h"

std::vector<Point> DeBoorsAlgo::createCurve(const std::vector<Point> &points) {
    std::vector<Point> result;

    size_t cur_step = 0;
    for (double t = 0; t <= 1; t += this->step) {
        result.emplace_back(getNextPoint(points, t, points.size(), cur_step));
        cur_step += 1;
    }

    return result;
}

double DeBoorsAlgo::calcNormalizedFunction(int i, int k, size_t point_count, double t) {
    if (k == 1) {
        double knot_first = getKnotValue(i);
        double knot_second = getKnotValue(i + 1);

        if (t >= knot_first && t < knot_second) {
            return 1;
        }

        return 0;
    }

    return divide(
            t - getKnotValue(i),
            getKnotValue(i + k - 1) - getKnotValue(i)
            ) * calcNormalizedFunction(i, k - 1, point_count, t) +
           divide(
           getKnotValue(i + k) - t,
           getKnotValue(i + k) - getKnotValue(i + 1)
           ) * calcNormalizedFunction(i + 1, k - 1, point_count, t);
}

Point DeBoorsAlgo::getNextPoint(const std::vector<Point> &points, double t, size_t full_points_count, size_t cur_step) {
    double cur_t = (cur_step / (1.0 / this->step)) + points.size() - 1;
    Point result_point(0, 0);

    for (int i = 0; i < points.size(); ++i) {
        double weight = calcNormalizedFunction(i + 1, points.size(), full_points_count, cur_t);

        result_point.setX(result_point.getX() + points[i].getX() * weight);
        result_point.setY(result_point.getY() + points[i].getY() * weight);
    }

    return result_point;
}

double DeBoorsAlgo::getKnotValue(int index) {
    return static_cast<double>(index - 1);
}

double DeBoorsAlgo::divide(double numerator, double denominator) {
    if (denominator == 0) {
        return 0;
    }

    return numerator / denominator;
}
