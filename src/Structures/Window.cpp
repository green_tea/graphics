#include "Window.h"

void Window::clearPixels() {
    for (int i = 0; i < WIDTH * HEIGHT * 4; i += 4) {
        this->bitmap->setIndex(i, {0, 255, 0, 100});
    }
}

sf::RenderWindow &Window::getWindow() {
    return this->window;
}

void Window::rightMousePress() {
    if (brushChecked()) {
        useCurrentBrush(getCurPoint());
        return;
    }

    if (start_xor_fill) {
         closeInFill();

        useCurrentBrush(getCurPoint());
        return;
    }

    if (right_mouse_pressed) {
        return;
    }

    right_mouse_pressed = true;

    start_mouse_pressed_point = getCurPoint();
}

void Window::rightMousePull() {
    if (!right_mouse_pressed) {
        return;
    }

    end_mouse_pressed_point = getCurPoint();

    right_mouse_pressed = false;

    if (curve_started) {
        curve_points.push_back(end_mouse_pressed_point);
        circles.emplace_back(end_mouse_pressed_point, 10);

        if (last_point == nullptr) {
            last_point = new Point(end_mouse_pressed_point);
            cur_start_point = end_mouse_pressed_point;
        } else {
            drawSimpleLine(*last_point, end_mouse_pressed_point, Color::getRed());
            delete last_point;
            last_point = new Point(end_mouse_pressed_point);
        }

        return;
    }

    if (count_down_started) {
        if (try_to_set_count_down_rectangle) {
            auto rectangle = new Rectangle(start_mouse_pressed_point, end_mouse_pressed_point);

            if (this->count_down_rect) {
                delete count_down_rect;
            }

            this->count_down_rect = rectangle;

            setCountDownRectangle(*rectangle);
        } else {
            drawSimpleLine(start_mouse_pressed_point, end_mouse_pressed_point, Color::getRed());
            addLineToCountDown(Line(start_mouse_pressed_point, end_mouse_pressed_point));
        }

        return;
    }

    if (selected_type == 1) {
        start_mouse_pressed_point = Point::generateRandomPoint(Window::WIDTH, Window::HEIGHT);
        end_mouse_pressed_point = Point::generateRandomPoint(Window::WIDTH, Window::HEIGHT);
    }

    if (finished_xor_fill) {
        finished_xor_fill = false;
        return;
    }

    if (line_fill_started) {
        if (last_point == nullptr) {
            last_point = new Point(end_mouse_pressed_point);
            cur_start_point = end_mouse_pressed_point;
        } else {
            drawSimpleLine(*last_point, end_mouse_pressed_point, Color::getRed());
            delete last_point;
            last_point = new Point(end_mouse_pressed_point);
        }
    }

    switch (selected_item) {
        case 1:
            this->drawBresenhamLine(start_mouse_pressed_point, end_mouse_pressed_point);

            break;
        case 2:
            drawSimpleLine(start_mouse_pressed_point, end_mouse_pressed_point);

            break;

        case 3:
            this->drawBresenhamLine(start_mouse_pressed_point, end_mouse_pressed_point);
            this->drawSimpleLine(start_mouse_pressed_point, end_mouse_pressed_point);

            break;

        case 4:
            this->drawBresenhamCircle(start_mouse_pressed_point, end_mouse_pressed_point);

            break;
        case 5:
            this->drawSimpleCircle(start_mouse_pressed_point, end_mouse_pressed_point);

            break;
        case 6:
            this->drawBresenhamCircle(start_mouse_pressed_point, end_mouse_pressed_point);
            this->drawSimpleCircle(start_mouse_pressed_point, end_mouse_pressed_point);

            break;

        case 7:
            this->drawBresenhamEllipse(start_mouse_pressed_point, end_mouse_pressed_point);

            break;
        case 8:
            this->drawSimpleEllipse(start_mouse_pressed_point, end_mouse_pressed_point);

            break;
        case 9:
            this->drawBresenhamEllipse(start_mouse_pressed_point, end_mouse_pressed_point);
            this->drawSimpleEllipse(start_mouse_pressed_point, end_mouse_pressed_point);

            break;
    }
}

void Window::drawPoints(const std::vector<Point>& points, const Color& color) {
    std::cout << "points len " << points.size() << "\n";

    for (auto point : points) {
        setPixel(color, point);
    }
}


void Window::setPixel(const Color& color, const Point& point) {
    if (!bitmap->isInside(point)) {
        return;
    }

    this->bitmap->setPixel(point, color);
}

void Window::drawPanel() {
    ImGui::Begin("Control Panel");

    if (count_down_started) {
        ImGui::ListBox("Count down types", &selected_count_down_strategy, count_down_types, 2);
        if (ImGui::Button("Process count down")) {
            this->drawCountDown();
        }

        if (!try_to_set_count_down_rectangle) {
            if (ImGui::Button("Set rectangle")) {
                this->try_to_set_count_down_rectangle = true;
            }
        } else {
            if (ImGui::Button("Set lines")) {
                this->try_to_set_count_down_rectangle = false;
            }
        }

        if (ImGui::Button("Go back")) {
            count_down_started = false;
            clear();
        }

        if (ImGui::Button("Clear all")) {
            lines.clear();
            clearCountDown();
        }
    } else if (curve_started) {
        if (this->curve_processor_string == "bsplain") {
            if (ImGui::Button("Go to Bezier")) {
                this->curve_processor_string = "bezier";
            }

            if (ImGui::Button("Draw Matrix b splain curve")) {
                this->drawCurve("matrix");
            }

            if (ImGui::Button("Draw Composite Matrix b splain curve")) {
                this->drawCurve("composite_matrix");
            }

            if (ImGui::Button("Draw Composite Matrix close b splain curve")) {
                this->drawCloseCurve("composite_matrix");
            }

            if (ImGui::Button("Draw De Boor b splain curve")) {
                this->drawCurve("de_boor");
            }

            if (ImGui::Button("Draw Composite De Boor b splain curve")) {
                this->drawCurve("composite_de_boor");
            }

            if (ImGui::Button("Draw Close Composite De Boor b splain curve")) {
                this->drawCloseCurve("composite_de_boor");
            }
        } else {
            if (ImGui::Button("Go to BSplain")) {
                this->curve_processor_string = "bsplain";
            }

            if (ImGui::Button("Draw De Casteljau bezier curve")) {
                this->drawCurve("DeCasteljau");
            }

            if (ImGui::Button("Draw matrix bezier curve")) {
                this->drawCurve("simple");
            }

            if (ImGui::Button("Draw De Casteljau close bezier curve")) {
                this->drawCloseCurve("DeCasteljau");
            }

            if (ImGui::Button("Draw Composite De Casteljau bezier curve")) {
                this->drawCurve("CompositeDeCasteljau");
            }

            if (ImGui::Button("Draw Composite De Casteljau close bezier curve")) {
                this->drawCloseCurve("CompositeDeCasteljau");
            }

            if (ImGui::Button("Draw matrix close bezier curve")) {
                this->drawCloseCurve("simple");
            }
        }

        if (ImGui::Button("Go back")) {
            this->curve_started = false;

            clear();
        }
    } else if (line_fill_started) {
        if (ImGui::Button("Xor Fill")) {
            this->start_xor_fill = true;
        }

        if (ImGui::Button("Close")) {
            closeInFill();
        }

        if (ImGui::Button("Go back")) {
            this->line_fill_started = false;
            start_xor_fill = false;

            clear();
        }
    } else {
        if (ImGui::Button("Start count down")) {
            lines.clear();

            count_down_started = true;
            // remove brush
            selected_item = 1;
        }

        if (ImGui::Button("Start Curves")) {
            curve_started = true;

            clear();
        }

        if (ImGui::Button("Start line fill")) {
            line_fill_started = true;

            clear();
        }

        ImGui::ListBox("Drawing", &selected_item, items, 10);
        ImGui::ListBox("Drawing type", &selected_type, drawing_types, 2);

        if (selected_item == 0) {
            ImGui::ListBox("Brushes", &selected_brush, brush_labels, 1);
            ImGui::ListBox("Colors", &selected_color, color_labels, 7);

            if (selected_brush == 0) {
                ImGui::ListBox("Brush strategies", &selected_brush_strategy, fill_strategy_labels, 2);
            }
        }

        if (ImGui::Button("About file")) {
            this->showFileInfo();
        }

        if (ImGui::Button("Upload file")) {
            this->uploadFile();
        }

        if (ImGui::Button("Clear")) {
            clear();
        }
    }

    ImGui::End();

    window.clear();

    texture.update(this->bitmap->getPixels());
    window.draw(sprite);

    drawLines();
    drawCircles();
    drawEllipses();
    if (count_down_rect) {
        drawSimpleRectangle(*this->count_down_rect, Color::getBlack());
    }
    if (curve_started) {
        drawPointsWithoutSave(draw_curve_points);
        drawPointsWithoutSave(curve_points);
    }

    ImGui::SFML::Render(window);
    window.display();
}

void Window::update(sf::Time time) {
    ImGui::SFML::Update(window, time);
}

void Window::processEvent(sf::Event event) {
    ImGui::SFML::ProcessEvent(event);
}

void Window::drawLines() {
    for (const auto& line : lines) {
        window.draw(line, 2, sf::Lines);
    }
}

void Window::drawCircles() {
    for (const auto& circle : circles) {
        sf::CircleShape circle_shape(circle.getRadius());
        circle_shape.setFillColor(sf::Color(0, 255, 0, 0));
        circle_shape.setPosition(circle.getCenterX() - circle.getRadius(), circle.getCenterY() - circle.getRadius());
        circle_shape.setOutlineThickness(1);
        circle_shape.setOutlineColor(sf::Color(255, 0, 0));

        window.draw(circle_shape);
    }
}

void Window::drawEllipses() {
    for (const auto& ellipse : ellipses) {
        EllipseShape ellipse_shape(sf::Vector2f(ellipse.getX(), ellipse.getY()), sf::Vector2f(ellipse.getA(), ellipse.getB()));
        ellipse_shape.setFillColor(sf::Color(0, 255, 0, 0));
        ellipse_shape.setOutlineThickness(1);
        ellipse_shape.setOutlineColor(sf::Color(255, 0, 0));
        window.draw(ellipse_shape);
    }
}

void Window::showFileInfo() {
    spdlog::info("Файл для загрузки графических примитивов задается в следующем виде: \n"
                 "Каждая строка содержит по одной фигуре. В строке должны присутствовать название фигуры \n"
                 "(Line/Circle/Ellipse) и координаты двух точек, которые описывают заданную фигуру");
}

void Window::uploadFile() {

}

void Window::drawBresenhamLine(const Point &p1, const Point &p2) {
    drawPoints(
            Line(p1, p2).getBresenham()
    );
}

void Window::drawSimpleLine(const Point &p1, const Point &p2, const Color& color) {
    lines.emplace_back(
            new sf::Vertex[2]{
                    sf::Vertex(p1.getVector2f(), color.getSFColor()),
                    sf::Vertex(p2.getVector2f(), color.getSFColor()),
            }
    );

    prepared_lines.emplace_back(p1, p2);
}

void Window::drawBresenhamCircle(const Point &p1, const Point &p2) {
    drawPoints(Circle(start_mouse_pressed_point, end_mouse_pressed_point).getBresenham());
}

void Window::drawSimpleCircle(const Point &p1, const Point &p2) {
    circles.emplace_back(start_mouse_pressed_point, end_mouse_pressed_point);
}

void Window::drawBresenhamEllipse(const Point &p1, const Point &p2) {
    ellipses.emplace_back(start_mouse_pressed_point, end_mouse_pressed_point);
}

void Window::drawSimpleEllipse(const Point &p1, const Point &p2) {
    drawPoints(Ellipse(start_mouse_pressed_point, end_mouse_pressed_point).getBresenham());
}

void Window::generateBrushes() {
    brushes = new AbstractBrush*[2] {new FillBrush(this->bitmap), new LineFillBrush(this->bitmap)};
    fill_strategies = new StrategyInterface*[2] {new DefaultFillStrategy(), new StringStrategy() };
    line_fill_strategies = new LineStrategyInterface*[1] {new XorFillStrategy() };
}

bool Window::brushChecked() {
    return selected_item == 0;
}

void Window::useCurrentBrush(const Point& point) {
    AbstractBrush* cur_brush = nullptr;

    if (start_xor_fill) {
        cur_brush = this->brushes[1];
    } else {
        cur_brush = this->brushes[selected_brush];
    }


    cur_brush->setColor(Color::getColor(this->color_labels[selected_color]));

    if (start_xor_fill) {
        cur_brush->setLines(this->prepared_lines);
    }

    if (cur_brush->withStrategy()) {
        if (!start_xor_fill) {
            (dynamic_cast<WithStrategyBrush*>(cur_brush))->setStrategy(getCurStrategy());
        } else {
            (dynamic_cast<LineStrategyBrush*>(cur_brush))->setStrategy(getCurLineStrategy());
        }
    }

    cur_brush->applyToPixel(point);

    if (start_xor_fill) {
        start_xor_fill = false;
        finished_xor_fill = true;
    }
}

StrategyInterface *Window::getCurStrategy() {
    return this->fill_strategies[selected_brush_strategy];
}

LineStrategyInterface *Window::getCurLineStrategy() {
    return this->line_fill_strategies[0];
}

Point Window::getCurPoint() {
    return {
        static_cast<double>(sf::Mouse::getPosition(window).x),
        static_cast<double>(sf::Mouse::getPosition(window).y)
    };
}

void Window::setCountDownType(int type) {
    switch (type) {
        case 0:
            this->countDown.setStrategy(StrategyManager::getWithTypesStrategy());
            return;
        case 1:
            this->countDown.setStrategy(StrategyManager::getFullStrategy());
            return;
    }
}

void Window::drawCountDown() {
    if (!count_down_rect) {
        return;
    }

    lines.clear();
    ellipses.clear();
    setCountDownType(selected_count_down_strategy);
    auto count_down_result = countDown.getResult();

    for (auto item : count_down_result.second) {
        drawSimpleLine(item.first.getStartPoint(), item.first.getFinishPoint(), item.second);
    }
}

void Window::drawSimpleRectangle(const Rectangle &rectangle, const Color& color) {
    for (int i = 0; i < 4; ++i) {
        Line line = rectangle.getLine(i);

        window.draw(new sf::Vertex[2]{
                sf::Vertex(line.getStartPoint().getVector2f(), color.getSFColor()),
                sf::Vertex(line.getFinishPoint().getVector2f(), color.getSFColor()),
        }, 2, sf::Lines);
    }
}

void Window::addLineToCountDown(const Line &line) {
    this->countDown.addLine(line);
}

void Window::setCountDownRectangle(const Rectangle &rectangle) {
    this->countDown.setRectangle(rectangle);
}

void Window::clearCountDown() {
    this->countDown.clear();

    if (count_down_rect) {
        delete count_down_rect;
        count_down_rect = nullptr;
    }
}

void Window::drawCloseCurve(const std::string& algo) {
    curve_processor->setProcessor(this->curve_processor_string);
    curve_processor->setAlgo(algo);

    draw_curve_points = curve_processor->getCloseCurve(curve_points);
}

void Window::drawCurve(const std::string& algo) {
    curve_processor->setProcessor(this->curve_processor_string);
    curve_processor->setAlgo(algo);

    draw_curve_points = curve_processor->getCurve(curve_points);
}

void Window::clear() {
    lines.clear();
    prepared_lines.clear();
    circles.clear();
    ellipses.clear();
    this->clearPixels();
    curve_points.clear();
    draw_curve_points.clear();
    count_down_rect = nullptr;
    if (last_point != nullptr) {
        delete last_point;
        last_point = nullptr;
    }
    clearCountDown();
}

void Window::drawPointsWithoutSave(const std::vector<Point>& points, const Color& color) {
    for (auto point : points) {
        window.draw(new sf::Vertex[1]{
                sf::Vertex(point.getVector2f(), color.getSFColor()),
        }, 1, sf::Points);
    }
}

void Window::closeInFill() {
    if (last_point == nullptr) {
        return;
    }

    this->drawSimpleLine(*last_point, cur_start_point);
    delete last_point;
    last_point = nullptr;
}
