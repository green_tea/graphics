#ifndef MYPROJECT_CUBECENTERMASSBUILDER_H
#define MYPROJECT_CUBECENTERMASSBUILDER_H

#include "../../../Point.h"
#include "../BuilderInterface.h"
#include "../../Cube.h"

class CubeCenterMassBuilder : public BuilderInterface {
public:
    void setStartPoint(const Point3D& point) override;
};


#endif //MYPROJECT_CUBECENTERMASSBUILDER_H
