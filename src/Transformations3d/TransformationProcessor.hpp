#ifndef MYPROJECT_TRANSFORMATIONPROCESSOR_HPP
#define MYPROJECT_TRANSFORMATIONPROCESSOR_HPP

#include "../Math/Matrix.hpp"
#include "TransformationInterface.h"
#include "Sequences/TransformationSequence.h"

#include <unordered_map>
#include <string>
#include <vector>

class TransformationProcessor {
private:
public:
    Matrix<double> processTransformation(TransformationInterface* transformation, const Matrix<double>& input) const {
        return transformation->process(input);
    };

    Matrix<double> processTransformationSequence(TransformationSequence* sequence, const Matrix<double>& input) const {
        Matrix<double> cur_matrix = input;

        auto transformations = sequence->iterate(input);

        for (auto transformation : transformations) {
            cur_matrix = processTransformation(transformation, cur_matrix);
        }

        return cur_matrix;
    }
};


#endif //MYPROJECT_TRANSFORMATIONPROCESSOR_HPP
