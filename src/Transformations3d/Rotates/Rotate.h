#ifndef MYPROJECT_ROTATE_H
#define MYPROJECT_ROTATE_H

#include "../../Math/Matrix.hpp"
#include "../TransformationInterface.h"

class Rotate : public TransformationInterface {
protected:
    double angle;

public:
    void setAngle(double angle);

    double getAngle() const;

    virtual Matrix<double> process(Matrix<double> value) const override = 0;
};


#endif //MYPROJECT_ROTATE_H
