#ifndef MYPROJECT_BEZIERPROCESSOR_H
#define MYPROJECT_BEZIERPROCESSOR_H

#include "BezierProcessor.h"

std::vector <Point> BezierProcessor::getCurve(const std::vector <Point> &points) {
    return algo->createCurve(points);
}

void BezierProcessor::setAlgo(const std::string& algo) {
    this->algo = algo_mapper[algo];
}

BezierProcessor::BezierProcessor() {
    algo_mapper["simple"] = new SimpleBezierAlgo();
    algo_mapper["DeCasteljau"] = new DeCasteljauAlgo();
    algo_mapper["CompositeDeCasteljau"] = new CompositeDeCasteljauAlgo();

    this->algo = algo_mapper["simple"];
}

std::vector<Point> BezierProcessor::getCloseCurve(const std::vector<Point>& points) {
    std::vector<Point> new_points(points.size());

    std::copy(points.begin(), points.end(), new_points.begin());
    new_points.emplace_back(points[0]);

    return this->algo->createCurve(new_points);
}

#endif //MYPROJECT_BEZIERPROCESSOR_H
