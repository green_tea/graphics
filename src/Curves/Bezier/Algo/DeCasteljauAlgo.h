#ifndef MYPROJECT_DECASTELJAUALGO_H
#define MYPROJECT_DECASTELJAUALGO_H

#include <algorithm>

#include "../../SteppedCurveAlgoInterface.h"

#include "../../../Primitives/Line.h"

class DeCasteljauAlgo : public SteppedCurveAlgoInterface {
private:
    Point getBezierPoint(const std::vector<Point>& points, double percent);
public:
    std::vector<Point> createCurve(const std::vector<Point> &points) override;
};


#endif //MYPROJECT_DECASTELJAUALGO_H
