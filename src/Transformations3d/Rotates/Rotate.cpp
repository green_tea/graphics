#include "Rotate.h"

void Rotate::setAngle(double angle) {
    this->angle = angle;
}

double Rotate::getAngle() const {
    return angle;
}
