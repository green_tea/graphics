#ifndef MYPROJECT_ABSTRACTBRUSH_H
#define MYPROJECT_ABSTRACTBRUSH_H

#include "../Primitives/Point.h"
#include "../Structures/Color.h"
#include "../Structures/Bitmap.h"
#include "../Primitives/Line.h"

#include <vector>

class AbstractBrush {
protected:
    Bitmap* bitmap;
    Color color;
    std::vector<Line> lines;

public:
    explicit AbstractBrush(Bitmap* const bitmap);

    virtual void applyToPixel(const Point& point) = 0;

    void setColor(const Color& color);

    virtual bool withStrategy();

    void setLines(std::vector<Line> lines);
};


#endif //MYPROJECT_ABSTRACTBRUSH_H
