#ifndef MYPROJECT_DEFAULTFILLSTRATEGY_H
#define MYPROJECT_DEFAULTFILLSTRATEGY_H

#include "../StrategyInterface.h"
#include "../../Structures/Bitmap.h"

#include <queue>

class DefaultFillStrategy : public StrategyInterface {
private:
    std::queue<Point> points;

    void fillAll(const Point& point);

    bool applyToPixel(const Point& point);
public:
    void operator()(const Point& point, Bitmap* bitmap, const Color& color) override;
};


#endif //MYPROJECT_DEFAULTFILLSTRATEGY_H
