#ifndef MYPROJECT_OBJECT3DINTERFACE_H
#define MYPROJECT_OBJECT3DINTERFACE_H

#include "../Line.h"
#include "../Point.h"
#include "../Face.h"
#include "Point3D.h"
#include "../../Transformations3d/TransformationInterface.h"
#include "../../Transformations3d/TransformationProcessor.hpp"
#include "../../Transformations3d/Rotates/RotateX.h"
#include "../../Transformations3d/Rotates/RotateY.h"
#include "../../Transformations3d/Rotates/RotateZ.h"
#include "../../Transformations3d/Translate.h"
#include "../../Transformations3d/Resize.h"
#include "../../Structures/Camera.h"
#include "../../Structures/Color.h"

#include <vector>
#include <unordered_map>
#include <string>
#include <algorithm>

class Object3DInterface {
private:
    TransformationSequence* current_sequence = nullptr;

    Color* color = nullptr;

private:

    std::vector<Point3D> applyTransformations() const;

    Point3D applyTransformationsToPoint(const Point3D& point) const;

    std::vector<Point3D> shiftAll(const std::vector<Point3D>& points) const;

public:
    constexpr static double const size = 100;

protected:
    Point3D start_point;

    TransformationProcessor processor;

    std::vector<Point3D> points;

    std::unordered_map<std::string, TransformationInterface*> transformations;

    bool remove_not_visible = true;

public:
    virtual std::vector<Line> getLines(const Camera& camera) const = 0;

    virtual std::vector<Face> getFaces(const Camera& camera) const = 0;

    std::vector<Face> getVisibleFaces(const Camera& camera) const;

    std::vector<Point> getPoints(const Camera& camera) const;

    std::vector<Point3D> getPointsWithoutProjection(const Camera& camera) const;

    std::vector<Line> getVisibleLines(const Camera& camera) const;

    Object3DInterface();

    void rotateX(double angle);

    void rotateY(double angle);

    void rotateZ(double angle);

    void translate(const Point3D& point);

    void resize(const Point3D& point);

    std::vector<Point3D> get3DPoints() const;

    void setSequence(TransformationSequence* sequence);

    bool sequenceStarted();

    bool isVisibleFace(const Face& face) const;

    Point3D getCenterPoint(const Camera& camera) const;

    void setColor(Color* color);

    Color* getColor() const;
};

#endif //MYPROJECT_OBJECT3DINTERFACE_H
