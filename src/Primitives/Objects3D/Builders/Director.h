//
// Created by green-tea on 11.03.2021.
//

#ifndef MYPROJECT_DIRECTOR_H
#define MYPROJECT_DIRECTOR_H

#include "BuilderInterface.h"
#include "Params/ParamsInterface.h"
#include "Params/StartPointParams.h"

class Director {
private:
    BuilderInterface* builder;

public:
    explicit Director(BuilderInterface* builder);

    Director();

    void setBuilder(BuilderInterface* builder);

    Object3DInterface* make(const std::string& type, ParamsInterface* params);
};


#endif //MYPROJECT_DIRECTOR_H
