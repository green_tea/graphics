#include "Torus.h"

Torus::Torus(const Point3D &start_point, double inner_radius, double outer_radius, int latitudes_count,
             int longitudes_count) : TorusInterface(start_point, [outer_radius, inner_radius, this](double i, double j) {
                 i -= 180;
                 j -= 180;

                 double x = (outer_radius + inner_radius * cos(toRadians(i))) * cos(toRadians(j));
                 double y = (outer_radius + inner_radius * cos(toRadians(i))) * sin(toRadians(j));
                 double z = inner_radius * sin(toRadians(i));

                return Point3D(x, y, z);
             }, inner_radius, outer_radius, latitudes_count, longitudes_count) {
    spins = 1;
}
