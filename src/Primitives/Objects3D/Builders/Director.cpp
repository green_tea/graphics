#include "Director.h"

Director::Director(BuilderInterface *builder) {
    this->builder = builder;
}

void Director::setBuilder(BuilderInterface *builder) {
    this->builder = builder;
}

Object3DInterface *Director::make(const std::string& type, ParamsInterface* params) {
    if (type == "start_point") {
            this->builder->setStartPoint(params->getStartPoint());

            return builder->getResult();
    }
}

Director::Director() {
    this->builder = nullptr;
}
