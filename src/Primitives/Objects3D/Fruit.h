#ifndef MYPROJECT_FRUIT_H
#define MYPROJECT_FRUIT_H

#include "SphereWithPolInterface.h"

class Fruit : public SphereWithPolInterface {
public:
    explicit Fruit(const Point3D& start_point, double radius, int latitudes_count, int longitudes_count);
};


#endif //MYPROJECT_FRUIT_H
