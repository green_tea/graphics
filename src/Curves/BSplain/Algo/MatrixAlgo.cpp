#include "MatrixAlgo.h"

std::vector<Point> MatrixAlgo::createCurve(const std::vector<Point> &points) {
    std::vector<Point> result_points;

    prepareMatrices(points);

    for (double t = 0; t <= 1; t += this->step) {
        result_points.push_back(processCurveStep(points, t));
    }

    return result_points;
}

void MatrixAlgo::prepareMatrices(const std::vector<Point>& points) {
    Matrix<double> matrix1(2, points.size());
    Matrix<double> matrix2(4, 4, {
            {1.0 / 6, -3.0 / 6, 3.0 / 6, -1.0 / 6},
            {4.0 / 6, 0, -1, 3.0 / 6},
            {1.0 / 6, 3.0 / 6, 3.0 / 6, -3.0 / 6},
            {0, 0, 0, 1.0 / 6}
    });

    for (int i = 0; i < points.size(); ++i) {
        matrix1.set(0, i, points[i].getX());
        matrix1.set(1, i, points[i].getY());
    }

    matrix4 = matrix1 * matrix2;
}

Point MatrixAlgo::processCurveStep(const std::vector<Point>& points, double t) {
    Matrix<double> matrix3(points.size(), 1);

    double cur_t = 1;
    for (int i = 0; i < points.size(); ++i) {
        matrix3.set(i, 0, cur_t);
        cur_t *= t;
    }

    Matrix<double> result = matrix4 * matrix3;

    return { result.get(0, 0), result.get(1, 0) };
}
