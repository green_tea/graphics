#include "Ellipse3DStartPointBuilder.h"

void Ellipse3DStartPointBuilder::setStartPoint(const Point3D &point) {
    this->object = new Ellipse3D(point, Ellipse3D::size, 36, 18);
}
