#ifndef MYPROJECT_STEPPEDCURVEALGO_H
#define MYPROJECT_STEPPEDCURVEALGO_H

#include "CurveAlgoInterface.h"

class SteppedCurveAlgoInterface : public CurveAlgoInterface {
protected:
    double step = 0.001;
};

#endif //MYPROJECT_STEPPEDCURVEALGO_H
