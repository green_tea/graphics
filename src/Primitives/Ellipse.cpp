#include "Ellipse.h"

Ellipse::Ellipse(Point start_point, Point finish_point) : start_point(start_point), finish_point(finish_point) {}

std::vector<Point> Ellipse::getBresenham() {
    std::vector<Point> points;

    int x = getXInt();
    int y = getYInt();

    int _x = 0;
    int _y = getBInt();

    int a_sqr = getAInt() * getAInt();
    int b_sqr = getBInt() * getBInt();

    // 4 * b^2 * (x+1)^2 + a^2 * (2 * y - 1)^2 - 4 * a^2 * b^2
    int delta = 4 * b_sqr * (_x + 1) * (_x + 1) + a_sqr * ((2 * _y - 1) * (2 * _y - 1)) - 4 * a_sqr * b_sqr;

    // a^2 * (2 * y - 1) > 2 * b^2 * (x + 1)
    while (a_sqr * (2 * _y - 1) > 2 * b_sqr * (_x + 1))
    {
        points.emplace_back(x + _x, y + _y);
        points.emplace_back(x + _x, y - _y);
        points.emplace_back(x - _x, y - _y);
        points.emplace_back(x - _x, y + _y);

        if (delta < 0)
        {
            _x++;
            // 4 * b^2 * (2 * x + 4)
            delta += 4 * b_sqr * (2 * _x + 3);
        }
        else
        {
            _x++;
            _y--;
            // 8 * a^2 * (y - 1) - 4 * b^2 * (2 * x + 4)
            delta -= 8 * a_sqr * _y - 4 * b_sqr * (2 * _x + 3);
        }
    }

    // b^2 * (2x + 1)^2 + 4 * a^2 * (y + 1)^2 - 4 * a^2 * b^2
    delta = b_sqr * (2 * _x + 1) * (2 * _x + 1) + 4 * a_sqr * (_y + 1) * (_y + 1) - 4 * a_sqr * b_sqr;
    // y >= 0
    while (_y >= 0)
    {
        points.emplace_back(x + _x, y + _y);
        points.emplace_back(x + _x, y - _y);
        points.emplace_back(x - _x, y - _y);
        points.emplace_back(x - _x, y + _y);

        if (delta < 0)
        {
            _y--;
            // 4 * a^2 * (2y + 4)
            delta += 4 * a_sqr * (2 * _y + 3);
        }
        else
        {
            _y--;
            _x++;
            // 8 * b^2 * x - 4 * a^2 * (2y + 4)
            delta -= 8 * b_sqr * _x - 4 * a_sqr * (2 * _y + 3);
        }
    }

    return points;
}

double Ellipse::getX() const {
    return (start_point.getX() + finish_point.getX()) / 2;
}

double Ellipse::getY() const {
    return (start_point.getY() + finish_point.getY()) / 2;
}

double Ellipse::getA() const {
    return std::abs(finish_point.getX() - start_point.getX()) / 2;
}

double Ellipse::getB() const {
    return std::abs(finish_point.getY() - start_point.getY()) / 2;
}

int Ellipse::getXInt() const {
    return static_cast<int>(std::round(getX()));
}

int Ellipse::getYInt() const {
    return static_cast<int>(std::round(getY()));
}

int Ellipse::getAInt() const {
    return static_cast<int>(std::round(getA()));
}

int Ellipse::getBInt() const {
    return static_cast<int>(std::round(getB()));
}
