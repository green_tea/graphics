#include "SimpleBezierAlgo.h"

#include "../../../Math/Matrix.hpp"
#include "../../../Math/helper.hpp"

std::vector<Point> SimpleBezierAlgo::createCurve(const std::vector<Point> &points) {
    std::vector<Point> ans;

    std::size_t n = points.size() - 1;

    Matrix<double> matrix1(2, points.size());
    Matrix<double> matrix2(points.size(), points.size());

    for (int i = 0; i < points.size(); ++i) {
        matrix1.set(0, i, points[i].getX());
        matrix1.set(1, i, points[i].getY());
    }

    for (int i = 0; i < points.size(); ++i) {
        for (int j = 0; j < points.size(); ++j) {
            double value = binPow(-1, abs(j - i)) * binCoef<double>(j, n) * binCoef(i, j);
            matrix2.set(i, j, value);
        }
    }

    Matrix<double> matrix4 = matrix1 * matrix2;

    for (double t = 0; t <= 1; t += step) {
        Matrix<double> matrix3(points.size(), 1);

        double cur_t = 1;
        for (int i = 0; i < points.size(); ++i) {
            matrix3.set(i, 0, cur_t);
            cur_t *= t;
        }

        auto res_matrix = matrix4 * matrix3;
        ans.emplace_back(res_matrix.get(0, 0), res_matrix.get(1, 0));
    }

    return ans;
}
