#ifndef MYPROJECT_ICOSAHEDRONSTARTPOINTBUILDER_H
#define MYPROJECT_ICOSAHEDRONSTARTPOINTBUILDER_H

#include "../BuilderInterface.h"
#include "../../Icosahedron.h"

class IcosahedronStartPointBuilder : public BuilderInterface {
public:
    void setStartPoint(const Point3D& point) override;
};


#endif //MYPROJECT_ICOSAHEDRON_H
