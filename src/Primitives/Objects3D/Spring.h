#ifndef MYPROJECT_SPRING_H
#define MYPROJECT_SPRING_H

#include "TorusInterface.h"

class Spring : public TorusInterface {
private:
    double curOuterRadius = -1;

public:
    Spring(const Point3D& start_point, double inner_radius, double outer_radius, int latitudes_count = 18, int longitudes_count = 9);
};


#endif //MYPROJECT_SPRING_H
