#ifndef MYPROJECT_SPIRAL_H
#define MYPROJECT_SPIRAL_H

#include "TorusInterface.h"

class Spiral : public TorusInterface {
private:
    double curOuterRadius = -1;

public:
    Spiral(const Point3D& start_point, double inner_radius, double outer_radius, int latitudes_count = 18, int longitudes_count = 9);
};


#endif //MYPROJECT_SPIRAL_H
