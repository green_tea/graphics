#ifndef MYPROJECT_EQUESTRIANSPIRALSTARTPOINTBUILDER_H
#define MYPROJECT_EQUESTRIANSPIRALSTARTPOINTBUILDER_H

#include "../BuilderInterface.h"
#include "../../EquestrianSpiral.h"

class EquestrianSpiralStartPointBuilder : public BuilderInterface {
public:
    void setStartPoint(const Point3D& point) override;

};


#endif //MYPROJECT_EQUESTRIANSPIRALSTARTPOINTBUILDER_H
