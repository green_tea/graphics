#ifndef MYPROJECT_SPHEREWITHPOLSTARTPOINTBUILDER_H
#define MYPROJECT_SPHEREWITHPOLSTARTPOINTBUILDER_H

#include "../BuilderInterface.h"
#include "../../SphereWithPol.h"

class SphereWithPolStartPointBuilder : public BuilderInterface {
public:
    void setStartPoint(const Point3D& point) override;
};


#endif //MYPROJECT_SPHEREWITHPOLSTARTPOINTBUILDER_H
