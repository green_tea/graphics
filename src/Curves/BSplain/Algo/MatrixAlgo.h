#ifndef MYPROJECT_MATRIXALGO_H
#define MYPROJECT_MATRIXALGO_H

#include "../../SteppedCurveAlgoInterface.h"

#include <vector>

#include "../../../Primitives/Point.h"
#include "../../../Math/Matrix.hpp"


class MatrixAlgo : public SteppedCurveAlgoInterface {
private:
    Matrix<double> matrix4;

protected:
    void prepareMatrices(const std::vector<Point>& points);

    Point processCurveStep(const std::vector<Point>& points, double t);

public:
    std::vector<Point> createCurve(const std::vector<Point>& points) override;
};


#endif //MYPROJECT_MATRIXALGO_H
