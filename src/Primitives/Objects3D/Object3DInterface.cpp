#include "Object3DInterface.h"

std::vector<Point3D> Object3DInterface::applyTransformations() const {
    std::vector<Point3D> new_points;

    for (auto point : points) {
        new_points.emplace_back(applyTransformationsToPoint(point));
    }

    if (current_sequence) {
        current_sequence->nextTick();
    }

    return new_points;
}

Object3DInterface::Object3DInterface() {
    transformations["rotateX"] = new RotateX();
    transformations["rotateY"] = new RotateY();
    transformations["rotateZ"] = new RotateZ();
    transformations["translate"] = new Translate();
    transformations["resize"] = new Resize();
}

std::vector<Point> Object3DInterface::getPoints(const Camera& camera) const {
    std::vector<Point3D> points = getPointsWithoutProjection(camera);

    std::vector<Point> result;
    result.reserve(points.size());

    for (auto point : points) {
        result.push_back(point.getProjection());
    }

    return result;
}

void Object3DInterface::rotateX(double angle) {
    double cur_angle = (dynamic_cast<RotateX&>(*transformations["rotateX"])).getAngle();
    (dynamic_cast<RotateX&>(*transformations["rotateX"])).setAngle(cur_angle + angle);
}

std::vector<Point3D> Object3DInterface::shiftAll(const std::vector<Point3D>& points) const {
    std::vector<Point3D> result;
    result.reserve(points.size());

    for (auto cur_point : points) {
        result.push_back(cur_point.shift(start_point));
    }

    return result;
}

std::vector<Point3D> Object3DInterface::get3DPoints() const {
    return points;
}

void Object3DInterface::rotateY(double angle) {
    double cur_angle = (dynamic_cast<RotateY&>(*transformations["rotateY"])).getAngle();
    (dynamic_cast<RotateY&>(*transformations["rotateY"])).setAngle(cur_angle + angle);
}

void Object3DInterface::rotateZ(double angle) {
    double cur_angle = (dynamic_cast<RotateZ&>(*transformations["rotateZ"])).getAngle();
    (dynamic_cast<RotateZ&>(*transformations["rotateZ"])).setAngle(cur_angle + angle);
}

Point3D Object3DInterface::applyTransformationsToPoint(const Point3D &point) const {
    Point3D new_point;

    if (current_sequence) {
        new_point = processor.processTransformationSequence(current_sequence, point.getMatrix());

        return new_point;
    }

    new_point = Point3D(processor.processTransformation(transformations.at("rotateX"), point.getMatrix()));
    new_point = Point3D(processor.processTransformation(transformations.at("rotateY"), new_point.getMatrix()));
    new_point = Point3D(processor.processTransformation(transformations.at("rotateZ"), new_point.getMatrix()));
    new_point = Point3D(processor.processTransformation(transformations.at("resize"), new_point.getMatrix()));
    new_point = Point3D(processor.processTransformation(transformations.at("translate"), new_point.getMatrix()));

    return new_point;
}

void Object3DInterface::translate(const Point3D& point) {
    Point3D cur_coordinates = (dynamic_cast<Translate&>(*transformations["translate"])).getCoordinates();
    (dynamic_cast<Translate&>(*transformations["translate"])).setCoordinates(cur_coordinates.shift(point));
}

void Object3DInterface::resize(const Point3D &point) {
    Point3D cur_coordinates = (dynamic_cast<Resize&>(*transformations["resize"])).getCoordinates();
    (dynamic_cast<Resize&>(*transformations["resize"])).setCoordinates(cur_coordinates.mult(point));
}

void Object3DInterface::setSequence(TransformationSequence* sequence) {
    this->current_sequence = sequence;
}

bool Object3DInterface::sequenceStarted() {
    return current_sequence != nullptr;
}

std::vector<Point3D> Object3DInterface::getPointsWithoutProjection(const Camera &camera) const {
    auto transformed_points = applyTransformations();

    std::vector<Point3D> result;
    result.reserve(transformed_points.size());


    for (auto point3D : transformed_points) {
        result.push_back(Point3D(camera.makeProjection(point3D.getMatrix())).shift(start_point));
    }

    return result;
}

std::vector<Line> Object3DInterface::getVisibleLines(const Camera &camera) const {
    std::vector<Face> faces = getVisibleFaces(camera);

    std::vector<Line> lines;

    for (auto& face : faces) {
        lines.push_back(face.getLine(0));
        lines.push_back(face.getLine(1));
        lines.push_back(face.getLine(2));
//        lines.emplace_back(
//            face.getCenter().getProjection(),
//            face.getCenter().shift(face.getNormal().resize(size / 2).toPoint()).getProjection()
//        );
    }

    auto last = std::unique(lines.begin(), lines.end(), [](const Line& lhs, const Line& rhs) {
        return lhs.getStartPoint() == rhs.getStartPoint() && lhs.getFinishPoint() == rhs.getFinishPoint();
    });
    lines.erase(last, lines.end());

    return lines;
}

bool Object3DInterface::isVisibleFace(const Face &face) const {
    return face.getNormal().scalarProduct({ 0, 0, 1 }) <= 0;
}

Point3D Object3DInterface::getCenterPoint(const Camera& camera) const {
    auto points = getPointsWithoutProjection(camera);
    Point3D start_point;

    for (auto point : points) {
        start_point = start_point.shift(point);
    }

    start_point.divide(points.size());

    return start_point;
}

void Object3DInterface::setColor(Color* color) {
    if (this->color != nullptr) {
        delete this->color;
    }

    this->color = color;
}

Color* Object3DInterface::getColor() const {
    return this->color;
}

std::vector<Face> Object3DInterface::getVisibleFaces(const Camera &camera) const {
    auto faces = getFaces(camera);

    std::vector<Face> result;

    auto center_point = getCenterPoint(camera);
    for (auto& face : faces) {
        if (face.calcValue(center_point) < 0) {
            face.rotate();
        }

        if (!remove_not_visible || isVisibleFace(face)) {
            result.push_back(face);
        };
    }

    return result;
}

