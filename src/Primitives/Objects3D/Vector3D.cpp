#include "Vector3D.h"

Vector3D::Vector3D(const Point3D &point) {
    this->x = point.getX();
    this->y = point.getY();
    this->z = point.getZ();
}

Vector3D::Vector3D(double x, double y, double z) {
    this->x = x;
    this->y = y;
    this->z = z;
}

Vector3D Vector3D::vectorProduct(const Vector3D &vector) const {
    return {
        y * vector.z - z * vector.y,
        -(x * vector.z - z * vector.x),
        x * vector.y - y * vector.x,
    };

}

Vector3D operator-(const Vector3D &lhs, const Vector3D &rhs) {
    return {
        lhs.x - rhs.x,
        lhs.y - rhs.y,
        lhs.z - rhs.z,
    };
}

double Vector3D::scalarProduct(const Vector3D &vector) const {
    return
        x * vector.x +
        y * vector.y +
        z * vector.z;
}

double Vector3D::getX() const {
    return x;
}

double Vector3D::getY() const {
    return y;
}

double Vector3D::getZ() const {
    return z;
}

Point3D Vector3D::toPoint() const {
    return { x, y, z };
}

double Vector3D::getSize() const {
    return sqrt(x * x + y * y + z * z);
}

Vector3D Vector3D::resize(double size) const {
    return { x / getSize() * size, y / getSize() * size, z / getSize() * size };
}

Vector3D::Vector3D(const Point3D& p1, const Point3D& p2) {
    x = p2.getX() - p1.getX();
    y = p2.getY() - p1.getY();
    z = p2.getZ() - p1.getZ();
}

Vector3D Vector3D::normalize() const {
    return resize(1);
}
