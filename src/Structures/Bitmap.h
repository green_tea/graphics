#ifndef MYPROJECT_BITMAP_H
#define MYPROJECT_BITMAP_H

#include <imgui-SFML.h>

#include "../Primitives/Point.h"

#include "Color.h"

class Bitmap {
private:
    sf::Uint8* pixels;
    double* depth;
    int width, height;

    int getIndexByPoint(const Point& point) const;

    int getDepthIndexByPoint(const Point& point) const;

public:
    Bitmap();

    Bitmap(int width, int height);

    double getDepth(int x, int y) const;

    void setDepth(int x, int y, double value);

    void setDepthByIndex(int index, double value);

    void setPixel(const Point& point, const Color& color);

    Color getPixel(const Point& point) const;

    void setIndex(int index, const Color& color);

    sf::Uint8* getPixels();

    bool isInside(const Point& point) const;

    bool isInsideDoubles(double x, double y) const;

    int getWidth();

    int getHeight();

    void clearDepth();
};


#endif //MYPROJECT_BITMAP_H
