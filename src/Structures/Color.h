#ifndef MYPROJECT_COLOR_H
#define MYPROJECT_COLOR_H

#include <string>

#include <SFML/Graphics/Color.hpp>

class Color {
private:
    int r, g, b, a;

public:
    static Color getRed();
    static Color getBlack();
    static Color getGreen();
    static Color getBlue();
    static Color getOrange();

    static Color getColor(const std::string& color_name);

    Color();

    Color(int r, int g, int b, int a);

    int getR() const;

    int getG() const;

    int getB() const;

    int getA() const;

    bool operator==(const Color& rhs);

    bool operator!=(const Color& rhs);

    sf::Color getSFColor() const;

    Color withIntensity(double intensity) const;
};


#endif //MYPROJECT_COLOR_H
