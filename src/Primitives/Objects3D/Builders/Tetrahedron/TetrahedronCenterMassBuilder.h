#ifndef MYPROJECT_CENTERMASSBUILDER_H
#define MYPROJECT_CENTERMASSBUILDER_H

#include "../../../Point.h"
#include "../BuilderInterface.h"
#include "../../Tetrahedron.h"

class TetrahedronCenterMassBuilder : public BuilderInterface {
public:
    void setStartPoint(const Point3D& point) override;
};


#endif //MYPROJECT_CENTERMASSBUILDER_H
