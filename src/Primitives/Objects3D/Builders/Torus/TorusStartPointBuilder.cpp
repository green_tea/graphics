#include "TorusStartPointBuilder.h"

void TorusStartPointBuilder::setStartPoint(const Point3D &point) {
    this->object = new Torus(point, Torus::size / 2, Torus::size, 32, 18);
}
