#include "SpringStartPointBuilder.h"

void SpringStartPointBuilder::setStartPoint(const Point3D &point) {
    this->object = new Spring(point, Spring::size / 2, Spring::size, 32, 18);
}
