#ifndef MYPROJECT_RECTANGLE_H
#define MYPROJECT_RECTANGLE_H

#include "Point.h"
#include "Line.h"

class Rectangle {
private:
    Point left_up_point;
    Point right_down_point;

public:
    Rectangle(const Point& left_up_point, const Point& right_down_point);

    const Point& getLeftUpPoint() const;

    const Point& getRightDownPoint() const;

    Line getLine(int number) const;
};


#endif //MYPROJECT_RECTANGLE_H
