#include "Moon.h"

Moon::Moon(const Point3D &start_point, double radius, int latitudes_count, int longitudes_count)
    : SphereWithPolInterface(start_point, [radius, this](double i, double j) {
    i -= 180;
    j -= 180;

    double x = radius * cos(toRadians(i)) * sin(toRadians(j));
    double y = radius * cos(toRadians(i)) * cos(toRadians(j));
    double z = radius * sin(toRadians(i));

    if (fabs(i) <= 45) {
        x = x + radius * (i / 45) * (i / 45);
    }

    z = 2 * z;

    return Point3D(x, y, z);
}, radius, latitudes_count, longitudes_count){ remove_not_visible = false; }