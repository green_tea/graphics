#ifndef MYPROJECT_ELLIPSE_H
#define MYPROJECT_ELLIPSE_H

#include "Point.h"

#include <vector>

class Ellipse {
private:
    Point start_point;
    Point finish_point;

public:
    Ellipse(Point start_point, Point finish_point);

    std::vector<Point> getBresenham();

    double getX() const;

    double getY() const;

    double getA() const;

    double getB() const;

    int getXInt() const;

    int getYInt() const;

    int getAInt() const;

    int getBInt() const;
};


#endif //MYPROJECT_ELLIPSE_H
