#ifndef MYPROJECT_TETRAHEDRONSTARTPOINTBUILDER_H
#define MYPROJECT_TETRAHEDRONSTARTPOINTBUILDER_H

#include "../../Tetrahedron.h"
#include "../BuilderInterface.h"

class TetrahedronStartPointBuilder : public BuilderInterface {
public:
    void setStartPoint(const Point3D& point) override;
};


#endif //MYPROJECT_TETRAHEDRONSTARTPOINTBUILDER_H
