#include "TetrahedronCenterMassBuilder.h"

void TetrahedronCenterMassBuilder::setStartPoint(const Point3D &point) {
    auto tmp_object = Tetrahedron(point);
    auto points = tmp_object.get3DPoints();

    Point3D center_point;
    for (const auto& cur_point : points) {
        center_point = center_point.shift(point);
    }

    center_point.divide(points.size());

    this->object = new Tetrahedron(point.shift(center_point.shift(point.minus())).minus());
}
