#include "Spiral.h"

Spiral::Spiral(const Point3D &start_point, double inner_radius, double outer_radius, int latitudes_count,
                                                 int longitudes_count)
    : TorusInterface(start_point, [outer_radius, inner_radius, this](double i, double j) {
    double k = 3;

    if (curOuterRadius == -1) {
        curOuterRadius = outer_radius;
    }

    double x = (curOuterRadius + inner_radius * sin(toRadians(j))) * cos(toRadians(i));
    double y = (curOuterRadius + inner_radius * sin(toRadians(j))) * sin(toRadians(i));
    double z = inner_radius * cos(toRadians(j));

    curOuterRadius = outer_radius * (1 + i / 360.0 + (static_cast<int>(i / 360) / k));

    return Point3D(x, y, z);
}, inner_radius, outer_radius, latitudes_count, longitudes_count) {
    remove_not_visible = false;
}
