#ifndef MYPROJECT_DEBOORSALGO_H
#define MYPROJECT_DEBOORSALGO_H

#include "../../SteppedCurveAlgoInterface.h"

class DeBoorsAlgo : public SteppedCurveAlgoInterface {
private:
    std::vector<Point> result_points;

protected:
    Point getNextPoint(const std::vector<Point>& points, double t, size_t full_points_count, size_t cur_step);

private:
    double calcNormalizedFunction(int i, int k, size_t point_count, double t);

    double getKnotValue(int index);

    double divide(double numerator, double denominator);

public:
    std::vector<Point> createCurve(const std::vector<Point>& points);
};


#endif //MYPROJECT_DEBOORSALGO_H
