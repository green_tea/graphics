#include "CountDownService.h"

CountDownService* CountDownService::instance = nullptr;

int CountDownService::getCode(const Rectangle &rectangle, const Point &point) const {
    if (point.getX() < rectangle.getLeftUpPoint().getX()) {
        if (point.getY() < rectangle.getLeftUpPoint().getY()) {
            return 9;
        }

        if (point.getY() <= rectangle.getRightDownPoint().getY()) {
            return 1;
        }

        return 5;
    }

    if (point.getX() <= rectangle.getRightDownPoint().getX()) {
        if (point.getY() < rectangle.getLeftUpPoint().getY()) {
            return 8;
        }

        if (point.getY() <= rectangle.getRightDownPoint().getY()) {
            return 0;
        }

        return 4;
    }

    if (point.getY() < rectangle.getLeftUpPoint().getY()) {
        return 10;
    }

    if (point.getY() <= rectangle.getRightDownPoint().getY()) {
        return 2;
    }

    return 6;
}

void CountDownService::shiftToLine(Line &line1, const Line &line2, bool is_first) const {
    if (is_first) {
        line1.setStartPoint(line1.intersect(line2));
    } else {
        line1.setFinishPoint(line1.intersect(line2));
    }
}

int CountDownService::getNearestLine(const Rectangle &rectangle, const Point &point) const {
    int code = getCode(rectangle, point);

    switch (code) {
        case 9:
        case 8:
        case 10:
            return 0;
        case 1:
        case 5:
            return 3;
        case 2:
        case 6:
            return 1;
        case 4:
            return 2;
        default:
            return -1;
    }
}

CountDownService *CountDownService::getInstance() {
    if (instance) {
        return instance;
    }

    return instance = new CountDownService();
}
