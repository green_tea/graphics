#include "Rectangle.h"

Rectangle::Rectangle(const Point &left_up_point, const Point &right_down_point) {
    this->left_up_point = left_up_point;
    this->right_down_point = right_down_point;
}

const Point &Rectangle::getLeftUpPoint() const {
    return left_up_point;
}

const Point &Rectangle::getRightDownPoint() const {
    return right_down_point;
}

Line Rectangle::getLine(int number) const {
    switch (number) {
        case 0:
            return { left_up_point, Point(right_down_point.getX(), left_up_point.getY()) };
        case 1:
            return { Point(right_down_point.getX(), left_up_point.getY()), right_down_point};
        case 2:
            return { right_down_point, Point(left_up_point.getX(), right_down_point.getY()) };
        case 3:
            return { Point(left_up_point.getX(), right_down_point.getY()), left_up_point };
        default:
            return { left_up_point, right_down_point };
            // throw something
    }
}
