#ifndef MYPROJECT_BEZIERPROCESSORTEST_H
#define MYPROJECT_BEZIERPROCESSORTEST_H

#include <string>
#include <unordered_map>

#include "../../Primitives/Point.h"

#include "../CurveAlgoInterface.h"
#include "Algo/SimpleBezierAlgo.h"
#include "Algo/DeCasteljauAlgo.h"
#include "Algo/CompositeDeCasteljauAlgo.h"
#include "../CurveProcessor.h"

class BezierProcessor : public CurveProcessorInterface {
private:
    CurveAlgoInterface* algo;
    std::unordered_map<std::string, CurveAlgoInterface*> algo_mapper;

public:
    BezierProcessor();

    std::vector<Point> getCurve(const std::vector<Point>& points) override;

    std::vector<Point> getCloseCurve(const std::vector<Point>& points) override;

    void setAlgo(const std::string& algo) override;
};


#endif //MYPROJECT_BEZIERPROCESSORTEST_H

