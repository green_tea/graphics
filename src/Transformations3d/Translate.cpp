#include "Translate.h"

Matrix<double> Translate::process(Matrix<double> value) const {
    Matrix<double> rotation_matrix = Matrix<double>(4, 4, {{
        {1, 0, 0, 0},
        {0, 1, 0, 0},
        {0, 0, 1, 0},
        {point.getX(), point.getY(), point.getZ(), 1},
    }});

    return value * rotation_matrix;
}

void Translate::setCoordinates(const Point3D& point) {
    this->point = point;
}

Point3D Translate::getCoordinates() const {
    return point;
}