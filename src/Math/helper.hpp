#ifndef MYPROJECT_HELPER_H
#define MYPROJECT_HELPER_H

template <typename T>
T binPow(T v, T p) {
    if (p == 0) {
        return 1;
    }

    if (p == 1) {
        return v;
    }

    T k = binPow(v, p / 2);
    return k * k * binPow(v, p % 2);
}

template <typename T>
T factorial(T v) {
    T ans = 1;

    for (T i = 2; i <= v; ++i) {
        ans *= i;
    }

    return ans;
}

template <typename T>
T binCoef(T k, T n) {
    if (k > n) {
        return 0;
    }

    return factorial(n) / (factorial(k) * factorial(abs(n - k)));
}



#endif //MYPROJECT_HELPER_H
