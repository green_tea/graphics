#include "DeCasteljauAlgo.h"

std::vector<Point> DeCasteljauAlgo::createCurve(const std::vector<Point> &points) {
    std::vector<Point> result;

    for (double t = 0; t <= 1; t += step) {
        result.push_back(getBezierPoint(points, t));
    }

    return result;
}

Point DeCasteljauAlgo::getBezierPoint(const std::vector<Point>& points, double percent) {
    if (points.size() == 1) {
        return points[0];
    }

    std::vector<Point> points_for_rec;

    for (int i = 1; i < points.size(); ++i) {
        points_for_rec.emplace_back(Line(points[i - 1], points[i]).getPoint(percent));
    }

    return getBezierPoint(points_for_rec, percent);
}
