#ifndef MYPROJECT_CAROUSEL_H
#define MYPROJECT_CAROUSEL_H

#include "../../Math/Matrix.hpp"
#include "../TransformationInterface.h"
#include "TransformationSequence.h"
#include "../Rotates/RotateY.h"
#include "../Translate.h"

#include <cmath>

class Carousel : public TransformationSequence {
private:
    double cur_angle = 0;

    const double rotate_angle = M_PI / 24;

    double shift_distance;

public:
    std::vector<TransformationInterface*> iterate(const Matrix<double>& input) override;

    void clear() override;

    void nextTick() override;

    explicit Carousel(double shift_distance);
};


#endif //MYPROJECT_CAROUSEL_H
