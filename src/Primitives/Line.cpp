
#include "Line.h"

Line::Line(Point start_point, Point finish_point) : start_point(start_point), finish_point(finish_point) {}

std::vector<Point> Line::getBresenham() {
    std::vector<Point> points;

    int x1 = start_point.getIntX();
    int y1 = start_point.getIntY();

    int x2 = finish_point.getIntX();
    int y2 = finish_point.getIntY();

    int x = x1;
    int y = y1;

    int delta_x = std::abs(x2 - x1);
    int delta_y = std::abs(y2 - y1);

    const int s1 = (x2 - x1) > 0 ? 1 : -1;
    const int s2 = (y2 - y1) > 0 ? 1 : -1;

    bool flag = true;
    bool changed = false;

    if (delta_y > delta_x) {
        changed = true;
        std::swap(delta_x, delta_y);
    }

    int e = 2 * delta_y - delta_x;

    int i = 1;
    while (i <= delta_x) {
        if (flag) {
            flag = false;
            points.emplace_back(x, y);
        }

        if (e > 0) {
            if (changed) {
                x += s1;
            } else {
                y += s2;
            }
            e -= 2 * delta_x;

            continue;
        }

        if (changed) {
            y += s2;
        } else {
            x += s1;
        }

        e += 2 * delta_y;
        ++i;

        flag = true;
    }

    return points;
}

Point Line::getStartPoint() const {
    return this->start_point;
}

Point Line::getFinishPoint() const {
    return this->finish_point;
}

void Line::setStartPoint(const Point& point) {
    this->start_point = point;
}

void Line::setFinishPoint(const Point &point) {
    this->finish_point = point;
}

std::tuple<double, double, double> Line::getEquation() const {
    double a = getStartPoint().getY() - getFinishPoint().getY();
    double b = getFinishPoint().getX() - getStartPoint().getX();
    double c = -a * getStartPoint().getX() - b * getStartPoint().getY();

    return std::make_tuple(a, b, c);
}

Point Line::intersect(const Line &line) {
    auto [a1, b1, c1] = getEquation();
    auto [a2, b2, c2] = line.getEquation();

    double x = - (c1 * b2 - c2 * b1) / (a1 * b2 - a2 * b1);
    double y = - (a1 * c2 - a2 * c1) / (a1 * b2  - a2 * b1);

    return { x, y };
}

Point Line::getPoint(double percent) const {
    double x = (finish_point.getX() - start_point.getX()) * percent + start_point.getX();
    double y = (finish_point.getY() - start_point.getY()) * percent + start_point.getY();

    return { x, y };
}

void Line::prepareCoordinates(bool is_x) {
    if (is_x) {
        if (this->getStartPoint().getX() > this->getFinishPoint().getX()) {
            Point tmp = this->getStartPoint();
            start_point = finish_point;
            finish_point = tmp;
        }

        return;
    }

    if (this->getStartPoint().getY() > this->getFinishPoint().getY()) {
        Point tmp = this->getStartPoint();
        start_point = finish_point;
        finish_point = tmp;
    }
}

Line::Line(double x1, double y1, double x2, double y2) {
    this->start_point = Point(static_cast<double>(x1), static_cast<double>(y1));
    this->finish_point = Point(static_cast<double>(x2), static_cast<double>(y2));
}
