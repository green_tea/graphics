#ifndef MYPROJECT_SPHEREWITHPOL_H
#define MYPROJECT_SPHEREWITHPOL_H

#include "SphereWithPolInterface.h"

#include <cmath>
#include <vector>

class SphereWithPol : public SphereWithPolInterface {
public:
    SphereWithPol(const Point3D& start_point, double radius = 50, int latitudes_count = 18, int longitudes_count = 9);
};


#endif //MYPROJECT_SPHEREWITHPOL_H
