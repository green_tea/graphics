#include "Spinner.h"

std::vector<TransformationInterface*> Spinner::iterate(const Matrix<double> &input) {
    std::vector<TransformationInterface*> result;

    TransformationInterface* rotate = new RotateY();

    (dynamic_cast<RotateY&>(*rotate)).setAngle(cur_angle);
    result.push_back(rotate);

    double x = shift_distance;// * cos(cur_angle);
    double z = 0;// * sin(cur_angle);

    TransformationInterface* translate = new Translate;

    (dynamic_cast<Translate&>(*translate)).setCoordinates({ x, 0, z });
    result.push_back(translate);

    return result;
}

Spinner::Spinner(double shift_distance) {
    this->shift_distance = shift_distance;
}

void Spinner::clear() {
    cur_angle = 0;
}

void Spinner::nextTick() {
    cur_angle += rotate_angle;
}
