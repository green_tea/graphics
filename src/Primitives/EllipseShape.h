#ifndef MYPROJECT_ELLIPSESHAPE_H
#define MYPROJECT_ELLIPSESHAPE_H

#include <SFML/Graphics/Shape.hpp>

#include <cmath>

class EllipseShape : public sf::Shape
{
public:
    void setRadius(const sf::Vector2f& radius)
    {
        m_radius = radius;
        update();
    }

    const sf::Vector2f& getRadius() const
    {
        return m_radius;
    }

    virtual std::size_t getPointCount() const
    {
        return 30;
    }

    sf::Vector2f getPoint(std::size_t index) const override
    {
        static const double pi = 3.141592654f;

        double angle = index * 2 * pi / getPointCount() - pi / 2;
        double x = std::cos(angle) * m_radius.x;
        double y = std::sin(angle) * m_radius.y;

        return sf::Vector2f(start.x + x, start.y + y);
    }

    explicit EllipseShape(const sf::Vector2f& start = sf::Vector2f(0, 0), const sf::Vector2f& radius = sf::Vector2f(0, 0)) :
            m_radius(radius), start(start)
    {
        update();
    }

    sf::Vector2f m_radius;
    sf::Vector2f start;
};

#endif //MYPROJECT_ELLIPSESHAPE_H
