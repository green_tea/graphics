#include "CompositeDeCasteljauAlgo.h"

#include <algorithm>

std::vector<Point> CompositeDeCasteljauAlgo::createCurve(const std::vector<Point> &points) {
    size_t chunk_size = 3;

    std::vector<Point> result;
    std::vector<Point> cur_chunk;

    for (const Point& point : points) {
        cur_chunk.emplace_back(point);

        if (cur_chunk.size() == chunk_size) {
            auto cur_curve = DeCasteljauAlgo::createCurve(cur_chunk);

            for (auto el : cur_curve) {
                result.push_back(el);
            }

            cur_chunk.clear();
            cur_chunk.push_back(point);
        }
    }

    if (!cur_chunk.empty()) {
        auto cur_curve = DeCasteljauAlgo::createCurve(cur_chunk);

        for (auto el : cur_curve) {
            result.push_back(el);
        }
    }

    return result;
}
