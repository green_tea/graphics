#ifndef MYPROJECT_STRINGSTRATEGY_H
#define MYPROJECT_STRINGSTRATEGY_H

#include "../StrategyInterface.h"

#include <queue>

class StringStrategy : public StrategyInterface {
private:
    std::queue<Point> points;

    void processLine(const Point& point);

    bool checkPoint(const Point& point);

    bool tryToAddPoint(const Point& point, bool flag);

    Point moveToEnd(Point point);

public:
    void operator()(const Point& point, Bitmap* bitmap, const Color& color) override;
};


#endif //MYPROJECT_STRINGSTRATEGY_H
