#ifndef MYPROJECT_SPINNER_H
#define MYPROJECT_SPINNER_H

#include "TransformationSequence.h"
#include "../Rotates/RotateY.h"
#include "../Translate.h"

#include <cmath>
#include <vector>

class Spinner : public TransformationSequence {
private:
    double cur_angle = 0;

    double shift_distance;

    const double rotate_angle = M_PI / 24 / 5;

public:
    std::vector<TransformationInterface*> iterate(const Matrix<double>& input) override;

    explicit Spinner(double shift_distance);

    void clear() override;

    void nextTick() override;
};


#endif //MYPROJECT_SPINNER_H
