#include "RotateY.h"

Matrix<double> RotateY::process(Matrix<double> value) const {
    Matrix<double> rotation_matrix = Matrix<double>(4, 4, {{
       {cos(angle), 0, -sin(angle), 0},
       {0, 1, 0, 0},
       {sin(angle), 0, cos(angle), 0},
       {0, 0, 0, 1},
    }});

    return value * rotation_matrix;
}
