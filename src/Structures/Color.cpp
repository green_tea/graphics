#include "Color.h"

Color Color::getRed() {
    return {255, 0, 0, 255};
}

Color::Color(int r, int g, int b, int a) : r(r), g(g), b(b), a(a) {}

Color Color::getBlack() {
    return {0, 0, 0, 255};
}

int Color::getR() const {
    return r;
}

int Color::getG() const {
    return g;
}

int Color::getB() const {
    return b;
}

int Color::getA() const {
    return a;
}

Color::Color() : r(0), g(0), b(0), a(255) {}

bool Color::operator==(const Color& color) {
    return this->r == color.r && this->g == color.g && this->b == color.b && this->a == color.a;
}

bool Color::operator!=(const Color& color) {
    return !(*this == color);
}

Color Color::getColor(const std::string& color_name) {
    if (color_name == "Red") {
        return { 255, 0, 0, 255 };
    }

    if (color_name == "Blue") {
        return { 0, 0, 255, 255 };
    }

    if (color_name == "Green") {
        return { 0, 255, 0, 255 };
    }

    if (color_name == "Black") {
        return { 0, 0, 0, 255 };
    }

    if (color_name == "White") {
        return { 255, 255, 255, 255 };
    }

    if (color_name == "Yellow") {
        return { 255, 255, 0, 255 };
    }

    if (color_name == "Orange") {
        return { 255, 200, 0, 255 };
    }

    return { 0, 0, 0, 255 };
}

Color Color::getGreen() {
    return {0, 255, 0, 255 };
}

Color Color::getBlue() {
    return { 0, 0, 255, 255 };
}

Color Color::getOrange() {
    return { 255, 165, 0, 255 };
}

sf::Color Color::getSFColor() const {
    return {
        static_cast<sf::Uint8>(getR()),
        static_cast<sf::Uint8>(getG()),
        static_cast<sf::Uint8>(getB()),
        static_cast<sf::Uint8>(getA())
    };
}

Color Color::withIntensity(double intensity) const {
    return {
        std::max(std::min(static_cast<int>(r * intensity), 255), 0),
        std::max(std::min(static_cast<int>(g * intensity), 255), 0),
        std::max(std::min(static_cast<int>(b * intensity), 255), 0),
        255
    };
}
