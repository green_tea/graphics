#include "Face.h"

Vector3D Face::getNormal() const {
    return { a, b, c };
}

Face::Face(const Point3D& p1, const Point3D& p2, const Point3D& p3) {
    this->p1 = p1;
    this->p2 = p2;
    this->p3 = p3;

    Vector3D v1 = Vector3D(p1);
    Vector3D v2 = Vector3D(p2);
    Vector3D v3 = Vector3D(p3);

    Vector3D v4 = v2 - v1;
    Vector3D v5 = v3 - v1;

    Vector3D v6 = v4.vectorProduct(v5);

    a = v6.getX();
    b = v6.getY();
    c = v6.getZ();

    d = -v6.scalarProduct(v1);
}

Line Face::getLine(int index) const {
    switch (index) {
        case 1:
            return {
                p1.getProjection(),
                p2.getProjection()
            };
        case 2:
            return {
                p2.getProjection(),
                p3.getProjection()
            };
        default:
            return {
                p3.getProjection(),
                p1.getProjection()
            };
    }
}

void Face::rotate() {
    a = -a;
    b = -b;
    c = -c;
    d = -d;
}

double Face::calcValue(const Point3D &point3D) const {
    return a * point3D.getX() + b * point3D.getY() + c * point3D.getZ() + d;
}

Point3D Face::getCenter() const {
    return {
        (p1.getX() + p2.getX() + p3.getX()) / 3,
        (p1.getY() + p2.getY() + p3.getY()) / 3,
        (p1.getZ() + p2.getZ() + p3.getZ()) / 3,
    };
}

Point3D Face::getPoint(int index) const {
    switch (index) {
        case 0:
            return p1;
        case 1:
            return p2;
        default:
            return p3;
    }
}

void Face::sortPoints(const std::function<bool(const Point3D &, const Point3D &)> &func) {
    if (func(p1, p2)) {
        std::swap(p1, p2);
    }

    if (func(p1, p3)) {
        std::swap(p1, p3);
    }

    if (func(p2, p3)) {
        std::swap(p2, p3);
    }
}

void Face::swapPoints(int f_index, int s_index) {
    std::swap(getPointRef(f_index), getPointRef(s_index));
}

Point3D &Face::getPointRef(int index) {
    switch (index) {
        case 0:
            return p1;
        case 1:
            return p2;
        default:
            return p3;
    }
}
