#ifndef MYPROJECT_CUBE_H
#define MYPROJECT_CUBE_H

#include "Object3DInterface.h"
#include "Point3D.h"
#include "../Point.h"
#include "../Face.h"

class Cube : public Object3DInterface {
public:
    std::vector<Line> getLines(const Camera& camera) const override;

    std::vector<Face> getFaces(const Camera& camera) const override;

    explicit Cube(Point3D point);
};


#endif //MYPROJECT_CUBE_H
