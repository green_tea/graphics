#ifndef MYPROJECT_LINEFILLBRUSH_H
#define MYPROJECT_LINEFILLBRUSH_H

#include "LineStrategyBrush.h"

class LineFillBrush : public LineStrategyBrush {
public:
    LineFillBrush(Bitmap* const bitmap);

    void applyToPixel(const Point& point) override;
};


#endif //MYPROJECT_LINEFILLBRUSH_H
