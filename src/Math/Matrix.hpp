#ifndef MYPROJECT_MATRIX_HPP
#define MYPROJECT_MATRIX_HPP

#include <vector>

template <typename T>
class Matrix {
private:
    T** matrix = nullptr;

    std::size_t n, m;

    void clear() {
        if (matrix == nullptr) {
            return;
        }

        for (int i = 0; i < n; ++i) {
            delete[] matrix[i];
        }

        delete[] matrix;
        matrix = nullptr;
    };

    void createNew() {
        this->matrix = new T*[n];
        for (int i = 0; i < n; ++i) {
            this->matrix[i] = new T[m];
        }
    };

    void copy(T** matrix) {
        this->matrix = new T*[n];
        for (int i = 0; i < n; ++i) {
            this->matrix[i] = new T[m];
            for (int j = 0; j < m; ++j) {
                this->matrix[i][j] = matrix[i][j];
            }
        }
    };

public:
    Matrix() {
        this->n = 0;
        this->m = 0;
        createNew();
    }

    Matrix(std::size_t n, std::size_t m) {
        setSizes(n, m);
    };

    Matrix(std::size_t n, std::size_t m, T** matrix) {
        this->matrix = matrix;
        this->n = n;
        this->m = m;
    };

    Matrix(std::size_t n, std::size_t m, std::vector<std::vector<T> > matrix) {
        this->matrix = new T*[n];
        for (int i = 0; i < n; ++i) {
            this->matrix[i] = new T[m];
            for (int j = 0; j < m; ++j) {
                this->matrix[i][j] = matrix[i][j];
            }
        }

        this->n = n;
        this->m = m;
    }

    ~Matrix() {
        clear();
    };

    Matrix(const Matrix<T>& matrix) {
        this->n = matrix.n;
        this->m = matrix.m;
        copy(matrix.matrix);
    };

//    Matrix(Matrix<T>&& matrix) {
//        this->matrix = matrix.matrix;
//        matrix.matrix = nullptr;
//    };

    Matrix& operator=(const Matrix<T>& matrix) {
        clear();
        copy(matrix.matrix);
        this->n = matrix.n;
        this->m = matrix.m;
        return *this;
    };

//    Matrix& operator=(Matrix<T>&& matrix) {
//        clear();
//        this->matrix = matrix.matrix;
//        this->n = matrix.n;
//        this->m = matrix.m;
//        matrix.matrix = nullptr;
//        return *this;
//    };

    template<typename H>
    friend Matrix<H> operator*(const Matrix<H>& lhs, const Matrix<H>& rhs) {
        H** new_matrix;
        new_matrix = new H*[lhs.n];
        for (int i = 0; i < lhs.n; ++i) {
            new_matrix[i] = new H[rhs.m];
        }

        for (int i = 0; i < lhs.n; ++i) {
            for (int j = 0; j < rhs.m; ++j) {
                new_matrix[i][j] = 0;
                for (int t = 0; t < lhs.m; ++t) {
                    new_matrix[i][j] += lhs.matrix[i][t] * rhs.matrix[t][j];
                }
            }
        }

        return { lhs.n, rhs.m, new_matrix };
    }

    void set(std::size_t x, std::size_t y, T value) {
        this->matrix[x][y] = value;
    };

    void setSizes(std::size_t n, std::size_t m) {
        this->n = n;
        this->m = m;
        createNew();
    };

    T get(std::size_t x, std::size_t y) const {
        return this->matrix[x][y];
    };
};


#endif //MYPROJECT_MATRIX_HPP
