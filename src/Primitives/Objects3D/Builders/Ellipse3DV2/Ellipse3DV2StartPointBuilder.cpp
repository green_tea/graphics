#include "Ellipse3DV2StartPointBuilder.h"

void Ellipse3DV2StartPointBuilder::setStartPoint(const Point3D &point) {
    this->object = new Ellipse3DV2(point, Ellipse3DV2::size, 36, 18);
}
