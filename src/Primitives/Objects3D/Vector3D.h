#ifndef MYPROJECT_VECTOR3D_H
#define MYPROJECT_VECTOR3D_H

#include "Point3D.h"

class Vector3D {
private:
    double x, y, z;

public:
    explicit Vector3D(const Point3D& point);

    explicit Vector3D(const Point3D& p1, const Point3D& p2);

    Vector3D(double x, double y, double z);

    Vector3D vectorProduct(const Vector3D& vector) const;

    friend Vector3D operator-(const Vector3D& lhs, const Vector3D& rhs);

    double scalarProduct(const Vector3D& vector) const;

    double getX() const;

    double getY() const;

    double getZ() const;

    Point3D toPoint() const;

    double getSize() const;

    Vector3D resize(double size) const;

    Vector3D normalize() const;
};


#endif //MYPROJECT_VECTOR3D_H
