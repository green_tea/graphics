#ifndef MYPROJECT_FACE_H
#define MYPROJECT_FACE_H

#include "../Math/Matrix.hpp"
#include "Objects3D/Vector3D.h"
#include "Objects3D/Point3D.h"
#include "Line.h"

#include <functional>

class Face {
private:
    Point3D p1, p2, p3;

    double a, b, c, d;

public:
    Vector3D getNormal() const;

    Face(const Point3D& p1, const Point3D& p2, const Point3D& p3);

    Line getLine(int index) const;

    void rotate();

    double calcValue(const Point3D& point3D) const;

    Point3D getCenter() const;

    Point3D getPoint(int index) const;

    Point3D& getPointRef(int index);

    void sortPoints(const std::function<bool(const Point3D&, const Point3D&)>& func);

    void swapPoints(int f_index, int s_index);
};


#endif //MYPROJECT_FACE_H
