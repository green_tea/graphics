#ifndef MYPROJECT_SPIRALSTARTPOINTBUILDER_H
#define MYPROJECT_SPIRALSTARTPOINTBUILDER_H

#include "../BuilderInterface.h"
#include "../../Spiral.h"

class SpiralStartPointBuilder : public BuilderInterface {
public:
    void setStartPoint(const Point3D& point) override;
};


#endif //MYPROJECT_SPIRALSTARTPOINTBUILDER_H
