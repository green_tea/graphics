#ifndef MYPROJECT_SPRINGSTARTPOINTBUILDER_H
#define MYPROJECT_SPRINGSTARTPOINTBUILDER_H

#include "../BuilderInterface.h"
#include "../../Spring.h"

class SpringStartPointBuilder : public BuilderInterface {
public:
    void setStartPoint(const Point3D& point) override;
};


#endif //MYPROJECT_SPRINGSTARTPOINTBUILDER_H
