#ifndef MYPROJECT_TRANSFORMATIONSEQUENCE_H
#define MYPROJECT_TRANSFORMATIONSEQUENCE_H

#include "../../Math/Matrix.hpp"
#include "../TransformationInterface.h"

#include <vector>

class TransformationSequence {
public:
    virtual std::vector<TransformationInterface*> iterate(const Matrix<double>& input) = 0;

    virtual void nextTick() = 0;

    virtual void clear() = 0;
};


#endif //MYPROJECT_TRANSFORMATIONSEQUENCE_H
