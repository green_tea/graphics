#include "SpiralStartPointBuilder.h"

void SpiralStartPointBuilder::setStartPoint(const Point3D &point) {
    this->object = new Spiral(point, Spiral::size / 2, Spiral::size, 32, 18);
}
