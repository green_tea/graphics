#include "Sphere.h"

std::vector<Line> Sphere::getLines(const Camera &camera) const {
    return {};
}

std::vector<Face> Sphere::getFaces(const Camera &camera) const {
    auto points = getPointsWithoutProjection(camera);

    std::vector<Face> faces;

    for (auto face : this->faces) {
        faces.emplace_back(points[face[0]], points[face[1]], points[face[2]]);
    }

    return faces;
}

Sphere::Sphere(const Point3D& start_point, int steps) {

    this->points = {
        {0.5 * size, 0.5 * size, -0.5 * size },
        {0.5 * size, -0.5 * size, 0.5 * size },
        {-0.5 * size, -0.5 * size, -0.5 * size },
        {-0.5 * size, 0.5 * size, 0.5 * size }
    };


//    double k = (1 + sqrt(5)) / 2;
//    points = {
//        { -size, 0, k * size },
//        { size, 0, k * size },
//        { 0, k * size, size },
//        { k * size, size, 0 },
//        { k * size, -size, 0 },
//        { size, 0, -k * size },
//        { 0, -k * size, -size },
//        { -size, 0, -k * size },
//        { -k * size, -size, 0 },
//        { -k * size, size, 0 },
//        { 0, k * size, -size },
//        { 0, -k * size, size },
//    };

//    faces = {
//        { 0, 1, 2 },
//        { 3, 2, 1 },
//        { 3, 1, 4 },
//        { 3, 4, 5 },
//        { 5, 4, 6 },
//        { 5, 6, 7 },
//        { 6, 8, 7 },
//        { 7, 8, 9 },
//        { 9, 8, 0 },
//        { 9, 0, 2 },
//        { 2, 10, 9 },
//        { 2, 3, 10 },
//        { 3, 5, 10 },
//        { 5, 7, 10 },
//        { 7, 9, 10 },
//        { 8, 6, 11 },
//        { 8, 11, 0 },
//        { 1, 0, 11 },
//        { 1, 11, 4 },
//        { 4, 11, 6 }
//    };

    faces = {
        { 0, 1, 2 },
        { 0, 1, 3 },
        { 1, 2, 3 },
        { 2, 0, 3 }
    };

    for (int c = 0; c < steps; ++c)
    {
        std::vector<std::vector<int>> cur_faces;

        for (auto face : faces)
        {
            Point3D p1 = points[face[0]].getMidPoint(points[face[1]]);
            Point3D p2 = points[face[1]].getMidPoint(points[face[2]]);
            Point3D p3 = points[face[2]].getMidPoint(points[face[0]]);

            points.push_back(p1);
            points.push_back(p2);
            points.push_back(p3);

            int size = points.size();
            cur_faces.push_back({ size - 3, size - 2, size - 1 });
            cur_faces.push_back({ face[0], size - 3, size - 1 });
            cur_faces.push_back({ size - 3, face[1], size - 2 });
            cur_faces.push_back({ size - 1, size - 2, face[2] });
        }

        std::copy(cur_faces.begin(), cur_faces.end(), std::back_inserter(faces));
    }

    this->start_point = { 0, 0, 0};
    normalize();
    this->start_point = start_point;
}

void Sphere::normalize() {
    for (auto& point : points) {
        Vector3D v = Vector3D(start_point, point).normalize().resize(size);

        point.set(start_point).shiftSelf(v.toPoint());
    }
}
