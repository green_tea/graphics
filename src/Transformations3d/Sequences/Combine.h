#ifndef MYPROJECT_COMBINE_H
#define MYPROJECT_COMBINE_H

#include "../../Math/Matrix.hpp"
#include "../TransformationInterface.h"
#include "TransformationSequence.h"
#include "../Rotates/RotateY.h"
#include "../Translate.h"

#include <cmath>

class Combine : public TransformationSequence {
private:
    double cur_angle = 0;

    double shift_distance;
    const double rotate_angle = M_PI / 24;

public:
    std::vector<TransformationInterface*> iterate(const Matrix<double>& input) override;

    void clear() override;

    void nextTick() override;

    explicit Combine(double shift_distance);
};


#endif //MYPROJECT_COMBINE_H
