#ifndef MYPROJECT_COUNTDOWNSTRATEGYINTERFACE_H
#define MYPROJECT_COUNTDOWNSTRATEGYINTERFACE_H

#include <vector>

#include "../../Primitives/Rectangle.h"
#include "../../Primitives/Line.h"

#include "../../Structures/Color.h"

#include "../Classes.h"

#include "Services/CountDownServiceInterface.h"
#include "Services/CountDownService.h"

class CountDownStrategyInterface {
protected:
    CountDownServiceInterface* service;

public:
    CountDownStrategyInterface() {
        this->service = static_cast<CountDownServiceInterface*>(CountDownService::getInstance());
    }

    virtual std::pair<Rectangle, std::vector<std::pair<Line, Color> > > getResult(
            const CountDown* count_down
        ) = 0;
};


#endif //MYPROJECT_COUNTDOWNSTRATEGYINTERFACE_H
