#ifndef MYPROJECT_BSPLAINPROCESSOR_H
#define MYPROJECT_BSPLAINPROCESSOR_H

#include "../CurveProcessor.h"

#include "../../Primitives/Point.h"

#include "../CurveAlgoInterface.h"

#include "Algo/MatrixAlgo.h"
#include "Algo/CompositeMatrixAlgo.h"
#include "Algo/DeBoorsAlgo.h"
#include "Algo/CompositeDeBoorsAlgo.h"

#include <vector>
#include <unordered_map>

class BSplainProcessor : public CurveProcessorInterface {
private:
    CurveAlgoInterface* algo;

    std::unordered_map<std::string, CurveAlgoInterface*> algo_mapper;

public:
    BSplainProcessor();

    std::vector<Point> getCurve(const std::vector<Point>& points) override;

    std::vector<Point> getCloseCurve(const std::vector<Point>& points) override;

    void setAlgo(const std::string& algo) override;
};


#endif //MYPROJECT_BSPLAINPROCESSOR_H
