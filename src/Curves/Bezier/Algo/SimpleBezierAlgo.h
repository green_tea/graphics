#ifndef MYPROJECT_SIMPLEBEZIERALGO_H
#define MYPROJECT_SIMPLEBEZIERALGO_H

#include "../../SteppedCurveAlgoInterface.h"

class SimpleBezierAlgo : public SteppedCurveAlgoInterface {
public:
    SimpleBezierAlgo() {
        this->step = 0.001;
    }

    std::vector<Point> createCurve(const std::vector<Point>& points) override;
};


#endif //MYPROJECT_SIMPLEBEZIERALGO_H
