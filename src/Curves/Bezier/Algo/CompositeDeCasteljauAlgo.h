#ifndef MYPROJECT_COMPOSITEDECASTELJAUALGO_H
#define MYPROJECT_COMPOSITEDECASTELJAUALGO_H

#include <algorithm>

#include "../../SteppedCurveAlgoInterface.h"

#include "../../../Primitives/Line.h"
#include "DeCasteljauAlgo.h"

class CompositeDeCasteljauAlgo : public DeCasteljauAlgo {
public:
    std::vector<Point> createCurve(const std::vector<Point> &points) override;
};


#endif //MYPROJECT_COMPOSITEDECASTELJAUALGO_H
