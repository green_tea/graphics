#include "Point3D.h"

Point3D::Point3D(double x, double y, double z) {
    this->x = x;
    this->y = y;
    this->z = z;
}

Point Point3D::getProjection() const {
    return Point(x, y);
}

Matrix<double> Point3D::getMatrix() const {
    return Matrix<double>(1, 4, {{x, y, z, 1.0}});
}

Point3D::Point3D(const Matrix<double>& matrix) {
    double h = matrix.get(0, 3);
    this->x = matrix.get(0, 0) / h;
    this->y = matrix.get(0, 1) / h;
    this->z = matrix.get(0, 2) / h;
}

Point3D Point3D::shift(const Point3D& point) const {
    return { x + point.x, y + point.y, z + point.z };
}

Point3D::Point3D() {
    x = 0;
    y = 0;
    z = 0;
}

void Point3D::divide(double v) {
    x /= v;
    y /= v;
    z /= v;
}

Point3D Point3D::minus() const {
    return { -x, -y, -z };
}

double Point3D::getX() const {
    return x;
}

double Point3D::getY() const {
    return y;
}

double Point3D::getZ() const {
    return z;
}

Point3D Point3D::mult(const Point3D &point) const {
    return { x * point.x, y * point.y, z * point.z };
}

Point3D &Point3D::operator=(const Point3D &point) {
    this->x = point.x;
    this->y = point.y;
    this->z = point.z;

    return *this;
}

Point3D Point3D::getMidPoint(const Point3D &point) const {
    return {
        (x + point.x) / 2,
        (y + point.y) / 2,
        (z + point.z) / 2,
    };
}

void Point3D::normalize() {
    double size = sqrt(x * x + y * y + z * z);
    divide(size);
}

void Point3D::shiftSelf(const Point3D &point) {
    x += point.x;
    y += point.y;
    z += point.z;
}

Point3D& Point3D::set(const Point3D& point) {
    x = point.x;
    y = point.y;
    z = point.z;

    return *this;
}

int Point3D::getIntX() {
    return static_cast<int>(x);
}

int Point3D::getIntY() {
    return static_cast<int>(y);
}

int Point3D::getIntZ() {
    return static_cast<int>(z);
}

double Point3D::getDistance(const Point3D& point) const {
    return sqrt((point.getX() - x) * (point.getX() - x) + (point.getY() - y) * (point.getY() - y) + (point.getZ() - z) * (point.getZ() - z));
}
