#ifndef MYPROJECT_DODECAHEDRONSTARTPOINTBUILDER_H
#define MYPROJECT_DODECAHEDRONSTARTPOINTBUILDER_H

#include "../BuilderInterface.h"
#include "../../Dodecahedron.h"

class DodecahedronStartPointBuilder : public BuilderInterface {
public:
    void setStartPoint(const Point3D& point) override;
};


#endif //MYPROJECT_DODECAHEDRONSTARTPOINTBUILDER_H
