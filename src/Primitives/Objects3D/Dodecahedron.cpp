#include "Dodecahedron.h"

std::vector<Line> Dodecahedron::getLines(const Camera &camera) const {
    auto points = getPoints(camera);

    return {
        { points[0], points[1] },
        { points[0], points[2] },
        { points[0], points[4] },
        { points[0], points[6] },
        { points[0], points[7] },
        { points[0], points[13] },
        { points[0], points[15] },

        { points[1], points[2] },
        { points[1], points[13] },
        { points[1], points[16] },
        { points[1], points[19] },

        { points[2], points[3] },
        { points[2], points[4] },
        { points[2], points[9] },
        { points[2], points[16] },
        { points[2], points[18] },

        { points[3], points[4] },
        { points[3], points[5] },
        { points[3], points[6] },
        { points[3], points[8] },
        { points[3], points[9] },

        { points[4], points[6] },
        { points[4], points[7] },

        { points[5], points[6] },
        { points[5], points[8] },
        { points[5], points[10] },
        { points[5], points[11] },

        { points[6], points[7] },
        { points[6], points[10] },
        { points[6], points[12] },
        { points[6], points[15] },

        { points[8], points[9] },
        { points[8], points[10] },
        { points[8], points[11] },
        { points[8], points[14] },
        { points[8], points[16] },
        { points[8], points[17] },

        { points[9], points[16] },
        { points[9], points[18] },

        { points[10], points[11] },
        { points[10], points[12] },
        { points[10], points[13] },
        { points[10], points[14] },
        { points[10], points[15] },

        { points[12], points[15] },

        { points[13], points[15] },
        { points[13], points[14] },
        { points[13], points[16] },
        { points[13], points[19] },

        { points[14], points[16] },
        { points[14], points[17] },

        { points[16], points[17] },
        { points[16], points[18] },
        { points[16], points[19] },
    };
}

Dodecahedron::Dodecahedron(const Point3D &point) {
    this->start_point = point;

    double k = (1 + sqrt(5)) / 2;
    this->points = {
            { -size, size, size },
            { -k * size, 0, 1 / k * size },
            { -size, -size, size },
            { size, -size, size },
            { 0, -1 / k * size, k * size },
            { k * size, 0, 1 / k * size },
            { size, size, size },
            { 0, 1 / k * size, k * size },
            { size, -size, -size },
            { 1 / k * size, -k * size, 0 },
            { size, size, -size },
            { k * size, 0, -1 / k * size },
            { 1 / k * size, k * size, 0 },
            { -size, size, -size },
            { 0, 1 / k * size, -k * size },
            { -1 / k * size, k * size, 0 },
            { -size, -size, -size },
            { 0, -1 / k * size, -k * size },
            { -1 / k * size, -k * size, 0 },
            { -k * size, 0, -1 / k * size },
    };
}

std::vector<Face> Dodecahedron::getFaces(const Camera &camera) const {
    auto points = getPointsWithoutProjection(camera);

    return {
            { points[2], points[3], points[4] },
            { points[0], points[7], points[6] },
            { points[0], points[2], points[4] },
            { points[0], points[4], points[7] },
            { points[3], points[6], points[4] },
            { points[6], points[7], points[4] },
            { points[6], points[3], points[5] },
            { points[10], points[11], points[8] },
            { points[3], points[8], points[5] },
            { points[5], points[8], points[11] },
            { points[6], points[5], points[10] },
            { points[10], points[5], points[11] },
            { points[0], points[15], points[13] },
            { points[6], points[10], points[12] },
            { points[15], points[0], points[6] },
            { points[12], points[15], points[6] },
            { points[13], points[10], points[15] },
            { points[10], points[12], points[15] },
            { points[10], points[14], points[13] },
            { points[8], points[16], points[17] },
            { points[14], points[17], points[16] },
            { points[14], points[16], points[13] },
            { points[14], points[8], points[17] },
            { points[14], points[10], points[8] },
            { points[3], points[9], points[8] },
            { points[2], points[16], points[18] },
            { points[2], points[18], points[9] },
            { points[3], points[2], points[9] },
            { points[9], points[16], points[8] },
            { points[9], points[18], points[16] },
            { points[19], points[13], points[16] },
            { points[0], points[1], points[2] },
            { points[1], points[13], points[19] },
            { points[1], points[0], points[13] },
            { points[1], points[16], points[2] },
            { points[1], points[19], points[16] },
    };
}
