#ifndef MYPROJECT_BLOBSTARTPOINTBUILDER_H
#define MYPROJECT_BLOBSTARTPOINTBUILDER_H

#include "../BuilderInterface.h"
#include "../../Blob.h"

class BlobStartPointBuilder : public BuilderInterface {
public:
    void setStartPoint(const Point3D& point) override;
};


#endif //MYPROJECT_BLOBSTARTPOINTBUILDER_H
