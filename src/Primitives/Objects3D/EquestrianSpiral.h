#ifndef MYPROJECT_CANNONICALSPIRAL_H
#define MYPROJECT_CANNONICALSPIRAL_H

#include "TorusInterface.h"

class EquestrianSpiral : public TorusInterface {
private:
    double curOuterRadius = -1;

public:
    EquestrianSpiral(const Point3D& start_point, double inner_radius, double outer_radius, int latitudes_count = 18, int longitudes_count = 9);
};


#endif //MYPROJECT_CANNONICALSPIRAL_H
