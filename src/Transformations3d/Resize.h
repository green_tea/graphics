#ifndef MYPROJECT_RESIZE_H
#define MYPROJECT_RESIZE_H

#include "../Primitives/Objects3D/Point3D.h"
#include "../Math/Matrix.hpp"
#include "TransformationInterface.h"

class Resize : public TransformationInterface {
private:
    Point3D point = { 1, 1, 1 };

public:
    Matrix<double> process(Matrix<double> value) const override;

    void setCoordinates(const Point3D& point);

    Point3D getCoordinates() const;
};


#endif //MYPROJECT_RESIZE_H
