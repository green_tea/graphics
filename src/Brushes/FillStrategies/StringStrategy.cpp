#include "StringStrategy.h"

void StringStrategy::operator()(const Point &point, Bitmap *bitmap, const Color &color) {
    this->color = color;
    this->start_color = bitmap->getPixel(point);
    this->bitmap = bitmap;

    this->points.push(point);

    while (!points.empty()) {
        processLine(moveToEnd(points.front()));
        points.pop();
    }
}

void StringStrategy::processLine(const Point& point) {
    bool flag1 = false;
    bool flag2 = false;

    Point cur_point = point;
    while (checkPoint(cur_point)) {
        flag1 |= tryToAddPoint({cur_point.getIntX(), cur_point.getIntY() + 1}, flag1);
        flag2 |= tryToAddPoint({cur_point.getIntX(), cur_point.getIntY() - 1}, flag2);
        this->bitmap->setPixel(cur_point, color);
        cur_point.decX();
    }
}

bool StringStrategy::checkPoint(const Point &point) {
    if (!bitmap->isInside(point)) {
        return false;
    }

    if (bitmap->getPixel(point) == color) {
        return false;
    }

    if (bitmap->getPixel(point) != start_color) {
        return false;
    }

    return true;
}

bool StringStrategy::tryToAddPoint(const Point &point, bool flag) {
    if (!flag && bitmap->isInside(point) && bitmap->getPixel(point) == start_color) {
        points.push(point);
        return true;
    }

    Point prev_point(point.getIntX() + 1, point.getIntY());
    if (!bitmap->isInside(prev_point) || bitmap->getPixel(prev_point) != start_color) {
        if (bitmap->isInside(point) && bitmap->getPixel(point) == start_color) {
            points.push(point);
            return true;
        }
    }

    return false;
}

Point StringStrategy::moveToEnd(Point point) {
    while (checkPoint(point)) {
        point.incX();
    }

    point.decX();
    return point;
}
