#ifndef MYPROJECT_MOONSTARTPOINTBUILDER_H
#define MYPROJECT_MOONSTARTPOINTBUILDER_H

#include "../BuilderInterface.h"
#include "../../Moon.h"

class MoonStartPointBuilder : public BuilderInterface {
public:
    void setStartPoint(const Point3D& point) override;
};


#endif //MYPROJECT_MOONSTARTPOINTBUILDER_H
