#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>

#include "Constants/MouseConstants.h"
#include "Structures/Window.h"
#include "Structures/Window3d.h"

int main() {
    Window3d window;

    sf::Mouse::setPosition(
            sf::Vector2i(MouseConstants::START_X_COORDINATE, MouseConstants::START_Y_COORDINATE),
            window.getWindow()
    );

    window.clearPixels();

    sf::Clock deltaClock;
    while (window.getWindow().isOpen()) {
        sf::Event event;

        while (window.getWindow().pollEvent(event)) {

            window.processEvent(event);

            if (event.type == sf::Event::Closed) {
                window.getWindow().close();
            }
        }

        if (sf::Mouse::isButtonPressed(sf::Mouse::Right)) {
            window.rightMousePress();
        } else {
            window.rightMousePull();
        }

        window.update(deltaClock.restart());
        window.drawPanel();
    }
}
