#ifndef MYPROJECT_TRANSLATE_H
#define MYPROJECT_TRANSLATE_H

#include "../Math/Matrix.hpp"
#include "TransformationInterface.h"
#include "../Primitives/Objects3D/Point3D.h"

class Translate : public TransformationInterface {
private:
    Point3D point;

public:
    Matrix<double> process(Matrix<double> value) const override;

    void setCoordinates(const Point3D& point);

    Point3D getCoordinates() const;
};


#endif //MYPROJECT_TRANSLATE_H
