#ifndef MYPROJECT_SPHEREWITHPOLINTERFACE_H
#define MYPROJECT_SPHEREWITHPOLINTERFACE_H

#include "Object3DInterface.h"

#include <functional>

class SphereWithPolInterface : public Object3DInterface {
protected:
    double radius;
    int latitudes_count;

    double toRadians(double angle) const {
        return M_PI / 180 * angle;
    }

public:
    std::vector<Line> getLines(const Camera& camera) const override;

    std::vector<Face> getFaces(const Camera& camera) const override;

    SphereWithPolInterface(
            const Point3D& start_point,
            std::function<Point3D(double i, double j)> func,
            double radius = 50,
            int latitudes_count = 18,
            int longitudes_count = 9
        );
};


#endif //MYPROJECT_SPHEREWITHPOLINTERFACE_H
