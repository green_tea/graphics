#ifndef MYPROJECT_PARAMSINTERFACE_H
#define MYPROJECT_PARAMSINTERFACE_H

class ParamsInterface {
public:
    virtual Point3D getStartPoint() const = 0;
};


#endif //MYPROJECT_PARAMSINTERFACE_H
