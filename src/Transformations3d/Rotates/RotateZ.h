#ifndef MYPROJECT_ROTATEZ_H
#define MYPROJECT_ROTATEZ_H

#include "Rotate.h"

#include <cmath>

class RotateZ : public Rotate {
public:
    Matrix<double> process(Matrix<double> value) const override;
};


#endif //MYPROJECT_ROTATEZ_H
