#include "CountDownStrategyFull.h"

std::pair<Rectangle, std::vector<std::pair<Line, Color> > >
CountDownStrategyFull::getResult(const CountDown *count_down) {
    result.clear();

    int count = 0;
    for (Line line : count_down->getLines()) {
        while (!tryToAddLine(count_down, line) && (count++ < 4));
        count = 0;
    }

    return std::pair<Rectangle, std::vector<std::pair<Line, Color>> >(count_down->getRectangle(), result);
}

bool CountDownStrategyFull::tryToAddLine(const CountDown* count_down, Line& line) {
    int code1 = this->service->getCode(count_down->getRectangle(), line.getStartPoint());
    int code2 = this->service->getCode(count_down->getRectangle(), line.getFinishPoint());

    if (code1 == 0 && code2 == 0) {
        result.emplace_back(line, Color::getBlack());
        return true;
    }

    if ((code1 & code2) == 0) {
        int nearest_line = service->getNearestLine(count_down->getRectangle(), line.getStartPoint());

        if (nearest_line == -1) {
            nearest_line = service->getNearestLine(count_down->getRectangle(), line.getFinishPoint());
            service->shiftToLine(line, count_down->getRectangle().getLine(nearest_line), false);

            return false;
        }

        service->shiftToLine(line, count_down->getRectangle().getLine(nearest_line), true);

        return false;
    }

    return true;
}