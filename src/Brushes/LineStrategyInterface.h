#ifndef MYPROJECT_LineSTRATEGYINTERFACE_H
#define MYPROJECT_LineSTRATEGYINTERFACE_H

#include "../Primitives/Line.h"
#include "../Structures/Bitmap.h"

class LineStrategyInterface {
protected:
    Bitmap* bitmap = nullptr;
    Color color;
    Color start_color;

public:
    virtual void operator()(const Point& point, Bitmap *bitmap, const std::vector<Line>& lines, const Color &color) = 0;
};


#endif //MYPROJECT_LineSTRATEGYINTERFACE_H
