#ifndef MYPROJECT_OCTAHEDRONSTARTPOINTBUILDER_H
#define MYPROJECT_OCTAHEDRONSTARTPOINTBUILDER_H

#include "../BuilderInterface.h"
#include "../../Octahedron.h"

class OctahedronStartPointBuilder : public BuilderInterface {
public:
    void setStartPoint(const Point3D& point) override;
};


#endif //MYPROJECT_OCTAHEDRONSTARTPOINTBUILDER_H
