#include "Fruit.h"

Fruit::Fruit(const Point3D &start_point, double radius, int latitudes_count, int longitudes_count)
    : SphereWithPolInterface(start_point, [radius, this](double i, double j) {
    i -= 180;
    j -= 180;

    double x = radius * cos(toRadians(i)) * sin(toRadians(j));
    double y = radius * cos(toRadians(i)) * cos(toRadians(j));
    double z = radius * sin(toRadians(i));

    x = x * (1 + 0.5 * fabs(sin(toRadians(2 * j))));
    y = y * (1 + 0.5 * fabs(sin(toRadians(2 * j))));

    return Point3D(x, y, z);
}, radius, latitudes_count, longitudes_count){ remove_not_visible = false; }
