#include "Blob.h"

Blob::Blob(const Point3D &start_point, double radius, int latitudes_count, int longitudes_count)
    : SphereWithPolInterface(start_point, [radius, this](double i, double j) {
    i -= 180;
    j -= 180;

    double x = radius * cos(toRadians(i)) * sin(toRadians(j));
    double y = radius * cos(toRadians(i)) * cos(toRadians(j));
    double z = radius * sin(toRadians(i));

    if (i > 0 && i <= 90) {
        z = z + radius * pow(i / 90, 4);
    }

    return Point3D(x, y, z);
}, radius, latitudes_count, longitudes_count){ remove_not_visible = true; }
