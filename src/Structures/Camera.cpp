#include "Camera.h"

Matrix<double> Camera::makeProjection(Matrix<double> v) const {
    if (this->perspectiveEnabled) {
        Matrix<double> perspective_projection_matrix(4, 4, {{
            {1, 0, 0, 0},
            {0, 1, 0, 0},
            {0, 0, 1, -1 / this->perspectiveCoefficient},
            {0, 0, 0, 1},
        }});
        v = v * perspective_projection_matrix;
        for (size_t i = 0; i < 4; ++i) {
            v.set(0, i, v.get(0, i) / v.get(0, 3));
        }
    } else {
        static const Matrix<double> orthogonal_projection_matrix(4, 4, {{
            {1, 0, 0, 0},
            {0, 1, 0, 0},
            {0, 0, 1, 0},
            {0, 0, 0, 1},
        }});
        v = v * orthogonal_projection_matrix;

        return v;
    }

    return toScreen(v);
}

Matrix<double> Camera::toScreen(Matrix<double> v) const {
    double xemax = static_cast<double>(this->width);
    double xemin = 0;
    double yemax = static_cast<double>(this->height);
    double yemin = 0;

    double xmax = xemax / 2.0;
    double xmin = -xmax;
    double ymax = yemax / 2.0;
    double ymin = -xmax;

    double kx = xmax - xmin == 0 ? std::numeric_limits<double>::infinity()
                                 : (xemax - xemin) / (xmax - xmin);
    double ky = ymax - ymin == 0 ? std::numeric_limits<double>::infinity()
                                 : (yemax - yemin) / (ymax - ymin);
    double k = std::min(kx, ky);
    k = k == std::numeric_limits<double>::infinity() ? 1 : k;

    TransformationInterface* resize = new Resize;
    (dynamic_cast<Resize&>(*resize)).setCoordinates({ k, k, k });
    v = this->transformation_processor.processTransformation(resize, v);

//    double dx = (xemin - k * xmin + xemax - k * xmax) / 2.0;
//    double dy = (yemin - k * ymin + yemax - k * ymax) / 2.0;

//    TransformationInterface* translate = new Translate;
//    (dynamic_cast<Translate&>(*translate)).setCoordinates({ dx, dy, 0 });
//    v = this->transformation_processor.processTransformation(translate, v);

//    delete translate;
//    delete resize;

    return v;
}

double Camera::getIntensity(const Face &face, const Point3D& point) const {
    Point3D p(500, 500, perspectiveCoefficient);

    double d = p.getDistance(point);

    Vector3D v = Vector3D(p, point).normalize();
    Vector3D n = face.getNormal().normalize();

    double O = n.scalarProduct(v);

    Vector3D v2 = Vector3D(2 * n.getZ() * n.getX(), 2 * n.getZ() * n.getY(), 2 * n.getZ() * n.getZ() - 1).normalize();

    double a = v2.scalarProduct(v);

    double intensity = IA * KA + IL / (d + K) * (KD * O + KS * pow(a, M));

    return intensity;
}
