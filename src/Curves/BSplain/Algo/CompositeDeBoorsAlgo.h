#ifndef MYPROJECT_COMPOSITEDEBOORSALGO_H
#define MYPROJECT_COMPOSITEDEBOORSALGO_H

#include "DeBoorsAlgo.h"

class CompositeDeBoorsAlgo : public DeBoorsAlgo {
private:
    void prepareChunk(std::vector<Point>& chunk);

    void swap(Point& p1, Point& p2);
public:
    std::vector<Point> createCurve(const std::vector<Point>& points) override;
};


#endif //MYPROJECT_COMPOSITEDEBOORSALGO_H
