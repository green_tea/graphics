#ifndef MYPROJECT_BUILDERINTERFACE_H
#define MYPROJECT_BUILDERINTERFACE_H

#include "../Object3DInterface.h"

class BuilderInterface {
protected:
    Object3DInterface* object = nullptr;

public:
    Object3DInterface* getResult() const {
        return this->object;
    };

    virtual void setStartPoint(const Point3D& point) = 0;

    void reset() {
        this->object = nullptr;
    }
};

#endif //MYPROJECT_BUILDERINTERFACE_H
