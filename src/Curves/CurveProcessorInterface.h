#ifndef MYPROJECT_CURVEPROCESSORINTERFACE_H
#define MYPROJECT_CURVEPROCESSORINTERFACE_H

#include "../Primitives/Point.h"

#include <vector>
#include <string>

class CurveProcessorInterface {
public:
    virtual std::vector<Point> getCurve(const std::vector<Point>& points) = 0;

    virtual std::vector<Point> getCloseCurve(const std::vector<Point>& points) = 0;

    virtual void setAlgo(const std::string& algo) = 0;
};

#endif //MYPROJECT_CURVEPROCESSORINTERFACE_H
