#include "WithStrategyBrush.h"

bool WithStrategyBrush::withStrategy() {
    return true;
}

void WithStrategyBrush::setStrategy(StrategyInterface *const strategy) {
    this->strategy = strategy;
}

WithStrategyBrush::WithStrategyBrush(Bitmap *bitmap) : AbstractBrush(bitmap) {}
