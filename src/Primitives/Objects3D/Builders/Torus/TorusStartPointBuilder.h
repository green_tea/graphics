#ifndef MYPROJECT_TORUSSTARTPOINTBUILDER_H
#define MYPROJECT_TORUSSTARTPOINTBUILDER_H

#include "../BuilderInterface.h"
#include "../../Torus.h"

class TorusStartPointBuilder : public BuilderInterface {
public:
    void setStartPoint(const Point3D& point) override;
};


#endif //MYPROJECT_TORUSSTARTPOINTBUILDER_H
