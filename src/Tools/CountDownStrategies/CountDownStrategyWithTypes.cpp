#include "CountDownStrategyWithTypes.h"

std::pair<Rectangle, std::vector<std::pair<Line, Color>>>
CountDownStrategyWithTypes::getResult(const CountDown *count_down) {

    std::vector<std::pair<Line, Color>> result;

    for (const Line& line : count_down->getLines()) {
        int code1 = service->getCode(count_down->getRectangle(), line.getStartPoint());
        int code2 = service->getCode(count_down->getRectangle(), line.getFinishPoint());

        if (code1 == 0 && code2 == 0) {
            result.emplace_back(line, Color::getRed());
        } else if ((code1 & code2) == 0) {
            result.emplace_back(line, Color::getOrange());
        } else {
            result.emplace_back(line, Color::getBlue());
        }
    }

    return std::pair<Rectangle, std::vector<std::pair<Line, Color> > >(count_down->getRectangle(), result);
}
