#include "Tetrahedron.h"

std::vector<Line> Tetrahedron::getLines(const Camera& camera) const {
    auto points = getPoints(camera);

    return {
        { points[0], points[1] },
        { points[0], points[2] },
        { points[1], points[2] },
        { points[2], points[3] },
        { points[0], points[3] },
        { points[1], points[3] },
    };
}

Tetrahedron::Tetrahedron(const Point3D& point) {
    this->start_point = point;
    this->points = {
            { 0, 0, 0 },
            { size, 0, 0 },
            { size / 2, sqrt(3) / 2 * size, 0 },
            { size / 2, sqrt(3) / 6 * size, 1 / sqrt(3) * size },
    };
}

std::vector<Face> Tetrahedron::getFaces(const Camera &camera) const {
    auto points = getPointsWithoutProjection(camera);

    return {
        { points[0], points[1], points[2] },
        { points[0], points[1], points[3] },
        { points[1], points[2], points[3] },
        { points[2], points[0], points[3] }
    };
}
