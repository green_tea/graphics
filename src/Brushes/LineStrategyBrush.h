#ifndef MYPROJECT_LINESTRATEGYBRUSH_H
#define MYPROJECT_LINESTRATEGYBRUSH_H

#include "LineStrategyInterface.h"
#include "AbstractBrush.h"

class LineStrategyBrush : public AbstractBrush {
protected:
    LineStrategyInterface* strategy = nullptr;

public:
    bool withStrategy() override;

    void setStrategy(LineStrategyInterface* const strategy);

    LineStrategyBrush(Bitmap* const bitmap);
};


#endif //MYPROJECT_LINESTRATEGYBRUSH_H
