#include "CountDown.h"

CountDown::CountDown(std::vector<Line> lines, Rectangle rectangle, CountDownStrategyInterface* strategy) :
                    lines(std::move(lines)),
                    rectangle(rectangle),
                    strategy(strategy) {}

void CountDown::addLine(const Line &line) {
    lines.push_back(line);
}

void CountDown::setRectangle(const Rectangle &rectangle) {
    this->rectangle = rectangle;
}

std::vector<Line> CountDown::getLines() const {
    return this->lines;
}

Rectangle CountDown::getRectangle() const {
    return this->rectangle;
}

std::pair<Rectangle, std::vector<std::pair<Line, Color> > > CountDown::getResult() const {
    return strategy->getResult(this);
}

void CountDown::setStrategy(CountDownStrategyInterface* strategy) {
    this->strategy = strategy;;
}

CountDown::CountDown(CountDownStrategyInterface *strategy) :
    rectangle(Point(0, 0), Point(0, 0)),
    strategy(strategy),
    lines(std::vector<Line>()) {}

void CountDown::clear() {
    this->setRectangle({ Point(0, 0), Point(0, 0) });
    this->lines.clear();
}

CountDown::CountDown() : CountDown(StrategyManager::getWithTypesStrategy()) {}
