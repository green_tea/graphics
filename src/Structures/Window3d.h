#ifndef MYPROJECT_WINDOW3D_H
#define MYPROJECT_WINDOW3D_H

#include <functional>
#include <cmath>
#include <iostream>
#include <string>
#include <unordered_map>
#include <sstream>

#include <imgui-SFML.h>
#include <imgui.h>

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/CircleShape.hpp>

#include <spdlog/spdlog.h>

#include "../Primitives/Point.h"
#include "../Primitives/Line.h"
#include "../Primitives/Circle.h"
#include "../Primitives/Ellipse.h"
#include "../Primitives/EllipseShape.h"
#include "../Primitives/Objects3D/Cube.h"
#include "../Primitives/Objects3D/Builders/Director.h"
#include "../Primitives/Objects3D/Builders/Cube/CubeStartPointBuilder.h"
#include "../Primitives/Objects3D/Builders/Tetrahedron/TetrahedronStartPointBuilder.h"
#include "../Primitives/Objects3D/Builders/Tetrahedron/TetrahedronCenterMassBuilder.h"
#include "../Primitives/Objects3D/Builders/Octahedron/OctahedronStartPointBuilder.h"
#include "../Primitives/Objects3D/Builders/Icosahedron/IcosahedronStartPointBuilder.h"
#include "../Primitives/Objects3D/Builders/Dodecahedron/DodecahedronStartPointBuilder.h"
#include "../Primitives/Objects3D/Builders/SphereWithPol/SphereWithPolStartPointBuilder.h"
#include "../Primitives/Objects3D/Builders/Sphere/SphereStartPointBuilder.h"
#include "../Primitives/Objects3D/Builders/Torus/TorusStartPointBuilder.h"
#include "../Primitives/Objects3D/Builders/Cube/CubeCenterMassBuilder.h"
#include "../Primitives/Objects3D/Builders/Garlic/GarlicStartPointBuilder.h"
#include "../Primitives/Objects3D/Builders/Blob/BlobStartPointBuilder.h"
#include "../Primitives/Objects3D/Builders/Ellipse3D/Ellipse3DStartPointBuilder.h"
#include "../Primitives/Objects3D/Builders/Ellipse3DV2/Ellipse3DV2StartPointBuilder.h"
#include "../Primitives/Objects3D/Builders/Fruit/FruitStartPointBuilder.h"
#include "../Primitives/Objects3D/Builders/Moon/MoonStartPointBuilder.h"
#include "../Primitives/Objects3D/Builders/Pear/PearStartPointBuilder.h"
#include "../Primitives/Objects3D/Builders/Spring/SpringStartPointBuilder.h"
#include "../Primitives/Objects3D/Builders/Spiral/SpiralStartPointBuilder.h"
#include "../Primitives/Objects3D/Builders/EquestrianSpiral/EquestrianSpiralStartPointBuilder.h"
#include "../Primitives/Objects3D/Point3D.h"

#include "../Brushes/AbstractBrush.h"
#include "../Brushes/StrategyInterface.h"
#include "../Brushes/FillBrush.h"
#include "../Brushes/FillStrategies/DefaultFillStrategy.h"
#include "../Brushes/FillStrategies/StringStrategy.h"
#include "../Brushes/FillStrategies/XorFillStrategy.h"
#include "../Brushes/LineFillBrush.h"

#include "../Tools/CountDown.h"

#include "../Curves/CurveProcessor.h"
#include "../Curves/CurveAlgoInterface.h"

#include "../Transformations3d/Rotates/RotateX.h"
#include "../Transformations3d/Rotates/RotateY.h"
#include "../Transformations3d/Rotates/RotateZ.h"

#include "../Transformations3d/Sequences/TransformationSequence.h"
#include "../Transformations3d/Sequences/Spinner.h"
#include "../Transformations3d/Sequences/Carousel.h"
#include "../Transformations3d/Sequences/Combine.h"

#include "Camera.h"
#include "Color.h"
#include "Bitmap.h"
#include "ZBuffer.h"

class BuilderMapperHasher {
public:
    bool operator()(const std::pair<std::string, std::string> v) const {
        return std::hash<std::string>{}(v.first) ^ (std::hash<std::string>{}(v.second) << 1);
    }
};

class Window3d {
public:
    static const int WIDTH = 1920;
    static const int HEIGHT = 1080;

    static const int FRAME_LIMIT = 60;

    std::unordered_map< std::pair<std::string, std::string>, BuilderInterface*, BuilderMapperHasher> builder_mapper;

    sf::RenderWindow window;
    sf::Texture texture;
    sf::Sprite sprite;

    Camera* camera;

    std::vector<Line> lines;

    Director director3D;

    Point3D center_point;
    std::vector<Circle> circles;

    Bitmap* bitmap;

    bool rotates_start = false;
    bool translates_start = false;
    bool resizes_start = false;
    bool sequences_start = false;
    bool set_colors_start = false;
    const double default_rotate_angle = M_PI / 12;
    const double default_translate = 5;
    const double default_resize = 0.95;
    const double default_projection_coef_shift = 100;

    bool lines_visible = true;
    bool light_turn_on = false;

    bool adding_figures = false;

    std::vector<double*> current_x_translates;
    double* current_x_translate = nullptr;

    Object3DInterface* current_object = nullptr;

    std::vector<Object3DInterface*> objects;

    long long cur_time = 0;
    long long sequence_timer = 10;

    ZBuffer z_buffer;

    explicit Window3d() : window(sf::VideoMode(WIDTH, HEIGHT), "SFML") {
        srand(time(0));
        this->window.setFramerateLimit(FRAME_LIMIT);

        ImGui::SFML::Init(window);

        texture.create(WIDTH, HEIGHT);
        this->sprite = sf::Sprite(texture);

        this->camera = new Camera(WIDTH, HEIGHT);

        this->bitmap = new Bitmap(Window3d::WIDTH, Window3d::HEIGHT);

        center_point = Point3D(500, 500, 0);

        builder_mapper[std::make_pair("cube", "start_point")] = new CubeStartPointBuilder;
        builder_mapper[std::make_pair("cube", "center_mass")] = new CubeCenterMassBuilder;
        builder_mapper[std::make_pair("tetrahedron", "start_point")] = new TetrahedronStartPointBuilder;
        builder_mapper[std::make_pair("tetrahedron", "center_mass")] = new TetrahedronCenterMassBuilder;
        builder_mapper[std::make_pair("octahedron", "start_point")] = new OctahedronStartPointBuilder;
        builder_mapper[std::make_pair("icosahedron", "start_point")] = new IcosahedronStartPointBuilder;
        builder_mapper[std::make_pair("dodecahedron", "start_point")] = new DodecahedronStartPointBuilder;
        builder_mapper[std::make_pair("sphere_with_pol", "start_point")] = new SphereWithPolStartPointBuilder;
        builder_mapper[std::make_pair("sphere", "start_point")] = new SphereStartPointBuilder;
        builder_mapper[std::make_pair("torus", "start_point")] = new TorusStartPointBuilder;
        builder_mapper[std::make_pair("garlic", "start_point")] = new GarlicStartPointBuilder;
        builder_mapper[std::make_pair("blob", "start_point")] = new BlobStartPointBuilder;
        builder_mapper[std::make_pair("ellipse_3d", "start_point")] = new Ellipse3DStartPointBuilder;
        builder_mapper[std::make_pair("ellipse_3d_v2", "start_point")] = new Ellipse3DV2StartPointBuilder;
        builder_mapper[std::make_pair("fruit", "start_point")] = new FruitStartPointBuilder;
        builder_mapper[std::make_pair("moon", "start_point")] = new MoonStartPointBuilder;
        builder_mapper[std::make_pair("pear", "start_point")] = new PearStartPointBuilder;
        builder_mapper[std::make_pair("spiral", "start_point")] = new SpiralStartPointBuilder;
        builder_mapper[std::make_pair("spring", "start_point")] = new SpringStartPointBuilder;
        builder_mapper[std::make_pair("equestrian_spiral", "start_point")] = new EquestrianSpiralStartPointBuilder;

        circles.emplace_back( center_point.getProjection(), 3);
    };

    sf::RenderWindow& getWindow();

    void rightMousePress();

    void rightMousePull();

    void drawPanel();

    void update(sf::Time time);

    void processEvent(sf::Event event);

    void clear();

    void clearPixels();

    void drawLines();

    void draw3dObject(const std::string& figure, const std::string& type);

    void drawCircles();

    void makeTransformation(const std::string &transformation);

    void makeSequence(const std::string& sequence);

    void finishSequence();

    void clearTransformations();

    void redraw();

    void drawAddingFiguresButtons();

    void drawSequencesButtons();

    void drawTranslatesButtons();

    void drawRotatesButtons();

    void drawResizesButtons();

    void drawColorButtons();

    void deleteObject(int index);

    void setColorToFigure(const Color& color);
};


#endif //MYPROJECT_WINDOW3D_H
