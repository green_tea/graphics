#ifndef MYPROJECT_CIRCLE_H
#define MYPROJECT_CIRCLE_H

#include <vector>

#include "Point.h"

class Circle {
private:
    Point point;
    double radius;

public:
    Circle(Point start_point, Point finish_point);

    Circle(Point point, float radius);

    std::vector<Point> getBresenham();

    double getRadius() const;

    double getCenterX() const;

    double getCenterY() const;

    int getRadiusInt() const;

    int getCenterXInt() const;

    int getCenterYInt() const;
};


#endif //MYPROJECT_CIRCLE_H
