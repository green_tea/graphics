#ifndef MYPROJECT_BLOB_H
#define MYPROJECT_BLOB_H

#include "SphereWithPolInterface.h"

class Blob : public SphereWithPolInterface {
public:
    explicit Blob(const Point3D& start_point, double radius, int latitudes_count, int longitudes_count);
};


#endif //MYPROJECT_BLOB_H
