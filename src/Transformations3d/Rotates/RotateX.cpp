#include "RotateX.h"

Matrix<double> RotateX::process(Matrix<double> value) const {
     Matrix<double> rotation_matrix = Matrix<double>(4, 4, {{
        {1, 0, 0, 0},
        {0, cos(angle), sin(angle), 0},
        {0, -sin(angle), cos(angle), 0},
        {0, 0, 0, 1},
    }});

    return value * rotation_matrix;
}
