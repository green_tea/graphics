#ifndef MYPROJECT_TORUSINTERFACE_H
#define MYPROJECT_TORUSINTERFACE_H

#include "Object3DInterface.h"

#include <functional>

class TorusInterface : public Object3DInterface {
protected:
    int latitudes_count;
    int longitudes_count;

    int spins = 3;

    double toRadians(double angle) {
        return M_PI / 180 * angle;
    }

public:
    std::vector<Line> getLines(const Camera& camera) const override;

    std::vector<Face> getFaces(const Camera& camera) const override;

    TorusInterface(const Point3D& start_point, std::function<Point3D(double, double)> func,
                   double inner_radius, double outer_radius, int latitudes_count = 18, int longitudes_count = 9);
};


#endif //MYPROJECT_TORUSINTERFACE_H
