#include "CubeCenterMassBuilder.h"

void CubeCenterMassBuilder::setStartPoint(const Point3D &point) {
    this->object = new Cube(point.shift({-Object3DInterface::size / 2, -Object3DInterface::size / 2, -Object3DInterface::size / 2}));
}
