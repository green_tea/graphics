#include "Carousel.h"

std::vector<TransformationInterface*> Carousel::iterate(const Matrix<double> &input) {
    std::vector<TransformationInterface*> result;

    TransformationInterface* translate = new Translate;

    (dynamic_cast<Translate&>(*translate)).setCoordinates({ shift_distance, 0, shift_distance });
    result.push_back(translate);

    TransformationInterface* rotate = new RotateY();

    (dynamic_cast<RotateY&>(*rotate)).setAngle(cur_angle);
    result.push_back(rotate);

    return result;
}

void Carousel::clear() {
    cur_angle = 0;
}

void Carousel::nextTick() {
    cur_angle += rotate_angle;
}

Carousel::Carousel(double shift_distance) {
    this->shift_distance = shift_distance;
}
