#ifndef MYPROJECT_SPHERE_H
#define MYPROJECT_SPHERE_H

#include "Object3DInterface.h"

#include <algorithm>

class Sphere : public Object3DInterface {
private:
    std::vector<std::vector<int>> faces;

    void normalize();

public:
    std::vector<Line> getLines(const Camera& camera) const override;

    std::vector<Face> getFaces(const Camera& camera) const override;

    Sphere(const Point3D& start_point, int steps = 6);
};


#endif //MYPROJECT_SPHERE_H
