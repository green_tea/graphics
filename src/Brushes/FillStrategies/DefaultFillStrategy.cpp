#include "DefaultFillStrategy.h"

void DefaultFillStrategy::operator()(const Point &point, Bitmap *bitmap, const Color& color) {
    this->bitmap = bitmap;
    this->color = color;
    this->start_color = bitmap->getPixel(point);

    fillAll(point);
}

void DefaultFillStrategy::fillAll(const Point &point) {
    this->points.push(point);

    while (!points.empty()) {
        Point cur_point = points.front();
        bool result = applyToPixel(cur_point);

        if (result) {
            this->points.emplace(cur_point.getIntX(), cur_point.getIntY() - 1);
            this->points.emplace(cur_point.getIntX() - 1, cur_point.getIntY());
            this->points.emplace(cur_point.getIntX(), cur_point.getIntY() + 1);
            this->points.emplace(cur_point.getIntX() + 1, cur_point.getIntY());
        }

        this->points.pop();
    }
}

bool DefaultFillStrategy::applyToPixel(const Point &point) {
    if (!bitmap->isInside(point)) {
        return false;
    }

    if (bitmap->getPixel(point) == color) {
        return false;
    }

    if (bitmap->getPixel(point) != start_color) {
        return false;
    }

    bitmap->setPixel(point, color);
    return true;
}
