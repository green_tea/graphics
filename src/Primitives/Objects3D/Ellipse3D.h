#ifndef MYPROJECT_ELLIPSE3D_H
#define MYPROJECT_ELLIPSE3D_H

#include "SphereWithPolInterface.h"

class Ellipse3D : public SphereWithPolInterface {
public:
    explicit Ellipse3D(const Point3D& start_point, double radius, int latitudes_count, int longitudes_count);
};


#endif //MYPROJECT_ELLIPSE3D_H
