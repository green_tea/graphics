#ifndef MYPROJECT_DODECAHEDRON_H
#define MYPROJECT_DODECAHEDRON_H

#include "Object3DInterface.h"

class Dodecahedron : public Object3DInterface {
public:
    std::vector<Line> getLines(const Camera& camera) const override;

    std::vector<Face> getFaces(const Camera& camera) const override;

    explicit Dodecahedron(const Point3D& point);
};


#endif //MYPROJECT_DODECAHEDRON_H
