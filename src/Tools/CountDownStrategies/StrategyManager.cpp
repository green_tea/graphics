#include "StrategyManager.h"

CountDownStrategyInterface* StrategyManager::with_types_strategy = nullptr;
CountDownStrategyInterface* StrategyManager::full_strategy = nullptr;

CountDownStrategyInterface *StrategyManager::getWithTypesStrategy() {
    if (StrategyManager::with_types_strategy) {
        return with_types_strategy;
    }

    return with_types_strategy = new CountDownStrategyWithTypes();
}

CountDownStrategyInterface *StrategyManager::getFullStrategy() {
    if (full_strategy) {
        return full_strategy;
    }

    return full_strategy = new CountDownStrategyFull();
}
