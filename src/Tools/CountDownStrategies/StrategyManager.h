#ifndef MYPROJECT_STRATEGYMANAGER_H
#define MYPROJECT_STRATEGYMANAGER_H

#include "CountDownStrategyInterface.h"
#include "CountDownStrategyWithTypes.h"
#include "CountDownStrategyFull.h"

class StrategyManager {
private:
    static CountDownStrategyInterface* with_types_strategy;
    static CountDownStrategyInterface* full_strategy;

public:
    static CountDownStrategyInterface* getWithTypesStrategy();

    static CountDownStrategyInterface* getFullStrategy();
};


#endif //MYPROJECT_STRATEGYMANAGER_H
