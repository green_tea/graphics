#ifndef MYPROJECT_COUNTDOWNSERVICEINTERFACE_H
#define MYPROJECT_COUNTDOWNSERVICEINTERFACE_H

#include "../../../Primitives/Rectangle.h"
#include "../../../Primitives/Point.h"
#include "../../../Primitives/Line.h"

class CountDownServiceInterface {
public:
    virtual int getCode(const Rectangle& rectangle, const Point& point) const = 0;

    virtual void shiftToLine(Line& line1, const Line& line2, bool is_first) const = 0;

    virtual int getNearestLine(const Rectangle& rectangle, const Point& point) const = 0;
};


#endif //MYPROJECT_COUNTDOWNSERVICEINTERFACE_H
