#include "BSplainProcessor.h"

std::vector<Point> BSplainProcessor::getCurve(const std::vector<Point> &points) {
    return this->algo->createCurve(points);
}

std::vector<Point> BSplainProcessor::getCloseCurve(const std::vector<Point> &points) {
    std::vector<Point> new_points(points.size());

    std::copy(points.begin(), points.end(), new_points.begin());
    new_points.emplace_back(points[0]);
    new_points.emplace_back(points[1]);
    new_points.emplace_back(points[2]);

    return this->algo->createCurve(new_points);
}

void BSplainProcessor::setAlgo(const std::string &algo) {
    this->algo = algo_mapper[algo];
}

BSplainProcessor::BSplainProcessor() {
    this->algo_mapper["matrix"] = new MatrixAlgo();
    this->algo_mapper["composite_matrix"] = new CompositeMatrixAlgo();
    this->algo_mapper["de_boor"] = new DeBoorsAlgo();
    this->algo_mapper["composite_de_boor"] = new CompositeDeBoorsAlgo();

    this->algo = this->algo_mapper["matrix"];
}


