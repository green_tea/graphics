#ifndef MYPROJECT_FRUITSTARTPOINTBUILDER_H
#define MYPROJECT_FRUITSTARTPOINTBUILDER_H

#include "../BuilderInterface.h"
#include "../../Fruit.h"

class FruitStartPointBuilder : public BuilderInterface {
public:
    void setStartPoint(const Point3D& point) override;
};


#endif //MYPROJECT_FRUITSTARTPOINTBUILDER_H
