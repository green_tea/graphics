#ifndef MYPROJECT_COUNTDOWNSTRATEGYFULL_H
#define MYPROJECT_COUNTDOWNSTRATEGYFULL_H

#include "CountDownStrategyInterface.h"

#include "../CountDown.h"

#include "../../Structures/Color.h"

#include <vector>

class CountDownStrategyFull : public CountDownStrategyInterface {
private:
    bool tryToAddLine(const CountDown* count_down, Line& line);

    std::vector<std::pair<Line, Color>> result;

public:

    std::pair<Rectangle, std::vector<std::pair<Line, Color> > > getResult(const CountDown* count_down) override;
};


#endif //MYPROJECT_COUNTDOWNSTRATEGYFULL_H
