#ifndef MYPROJECT_POINT3D_H
#define MYPROJECT_POINT3D_H

#include "../Point.h"
#include "../../Math/Matrix.hpp"

class Point3D {
private:
    double x, y, z;

public:
    Point3D();

    Point3D(double x, double y, double z);

    Point3D& operator=(const Point3D& point);

    Point3D(const Matrix<double>& matrix);

    Point getProjection() const;

    Matrix<double> getMatrix() const;

    Point3D shift(const Point3D& point) const;

    void shiftSelf(const Point3D& point);

    Point3D mult(const Point3D& point) const;

    void divide(double v);

    Point3D minus() const;

    double getX() const;

    double getY() const;

    double getZ() const;

    Point3D getMidPoint(const Point3D& point) const;

    void normalize();

    Point3D& set(const Point3D& point);

    int getIntX();

    int getIntY();

    int getIntZ();

    double getDistance(const Point3D& point) const;
};


#endif //MYPROJECT_POINT3D_H
