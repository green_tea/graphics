#ifndef MYPROJECT_TORUS_H
#define MYPROJECT_TORUS_H

#include "TorusInterface.h"

class Torus : public TorusInterface {
public:
    Torus(const Point3D& start_point, double inner_radius, double outer_radius, int latitudes_count = 18, int longitudes_count = 9);
};


#endif //MYPROJECT_TORUS_H
