#include "GarlicStartPointBuilder.h"

void GarlicStartPointBuilder::setStartPoint(const Point3D &point) {
    this->object = new Garlic(point, Garlic::size, 32, 18);
}
