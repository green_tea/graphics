#include "TorusInterface.h"

std::vector<Line> TorusInterface::getLines(const Camera &camera) const {
    return {};
}

std::vector<Face> TorusInterface::getFaces(const Camera &camera) const {
    std::vector<Face> faces;
    auto points = getPointsWithoutProjection(camera);

    for (int i = 0; i + latitudes_count + 2 < points.size(); ++i) {
        faces.emplace_back(points[i], points[i + 1], points[i + latitudes_count + 1]);
        faces.emplace_back(points[i + 1], points[i + latitudes_count + 1], points[i + latitudes_count + 2]);
    }

    return faces;
}

TorusInterface::TorusInterface(const Point3D &start_point, std::function<Point3D(double, double)> func,
                               double inner_radius, double outer_radius, int latitudes_count, int longitudes_count) {
    remove_not_visible = false;
    this->latitudes_count = latitudes_count;
    this->longitudes_count = longitudes_count;

    double latitude_step = 360.0 / latitudes_count;
    double longitude_step = 360.0 / longitudes_count;

    for (double i = 0; i <= spins * 360.0; i += longitude_step)
    {
        for (double j = 0; j <= 360.0; j += latitude_step)
        {
            points.push_back(func(i, j));
        }
    }

    this->start_point = start_point;
}
