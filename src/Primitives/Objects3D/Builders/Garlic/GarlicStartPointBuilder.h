#ifndef MYPROJECT_GARLICSTARTPOINTBUILDER_H
#define MYPROJECT_GARLICSTARTPOINTBUILDER_H

#include "../BuilderInterface.h"
#include "../../Garlic.h"

class GarlicStartPointBuilder : public BuilderInterface {
public:
    void setStartPoint(const Point3D& point) override;
};


#endif //MYPROJECT_GARLICSTARTPOINTBUILDER_H
