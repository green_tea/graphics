#ifndef MYPROJECT_TETRAHEDRON_H
#define MYPROJECT_TETRAHEDRON_H

#include "Object3DInterface.h"
#include "Point3D.h"

#include <cmath>

class Tetrahedron : public Object3DInterface {
public:
    std::vector<Line> getLines(const Camera& camera) const override;

    std::vector<Face> getFaces(const Camera& camera) const override;

    explicit Tetrahedron(const Point3D& point);
};


#endif //MYPROJECT_TETRAHEDRON_H
