#ifndef MYPROJECT_ELLIPSE3DV2_H
#define MYPROJECT_ELLIPSE3DV2_H

#include "SphereWithPolInterface.h"

class Ellipse3DV2 : public SphereWithPolInterface {
public:
    explicit Ellipse3DV2(const Point3D& start_point, double radius, int latitudes_count, int longitudes_count);
};


#endif //MYPROJECT_ELLIPSE3DV2_H
