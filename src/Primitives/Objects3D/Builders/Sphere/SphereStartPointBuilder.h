#ifndef MYPROJECT_SPHERESTARTPOINTBUILDER_H
#define MYPROJECT_SPHERESTARTPOINTBUILDER_H

#include "../BuilderInterface.h"
#include "../../Sphere.h"

class SphereStartPointBuilder : public BuilderInterface {
public:
    void setStartPoint(const Point3D& point) override;
};


#endif //MYPROJECT_SPHERESTARTPOINTBUILDER_H
